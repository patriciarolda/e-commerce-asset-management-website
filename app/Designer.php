<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designer extends Model
{
    public function designer(){
        return $this->hasMany('App\Gown', 'id');
    }
}
