<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pickup extends Model
{
    public function gown(){
        return $this->belongsTo('App\Gown', 'id');
    }

    public function action(){
        return $this->belongsToMany('App\Action', 'id');
    }

    public function user(){
        return $this->belongsToMany('App\User', 'id');
    }
}

