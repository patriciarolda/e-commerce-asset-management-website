<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gown;
use Session;
use App\Category;
use Auth;
use App\Order;
use App\Pickup;
use App\Date;
use DB;

class CartController extends Controller
{
    public function add(Request $request, $id)
    {
        
        $gown_id = $id;
        // dd($gown_id);
         if($request->session()->exists('cart.'.$gown_id)){
            $currentQuantity = $request->session()->get('cart.'.$gown_id.'.quantity');
            $totalQuantity = $request->qty + $currentQuantity;

            // $cart = new Cart;
            // $pickup->user_id = Auth::user()->id;
            $request->session()->put('cart.'.$gown_id.'.quantity', "$totalQuantity");
            $request->session()->put('cart.'.$gown_id.'.date', "$request->date");
            $request->session()->put('cart.'.$gown_id.'.requirements', "$request->requirements");
            // $cart->save();
        }else{
            $request->session()->put('cart.'.$gown_id.'.quantity', "$request->qty");
            $request->session()->put('cart.'.$gown_id.'.date', "$request->date");
            $request->session()->put('cart.'.$gown_id.'.requirements', "$request->requirements");          
        }
        Session::flash('success', 'Item(s) added to cart');
        return redirect('cart/cart');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        $cart = session()->get('cart');

        $item_ids = (!empty($cart)) ? array_keys($cart):[];
        $gowns = Gown::find($item_ids);


        return view('/cart/cart',['gowns'=>$gowns]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function remove(Request $request)
    {
        $gown_id = $request->gown_id;

        $request->session()->forget('cart.'. $gown_id);
        return redirect('cart/cart');

    }

    public function empty(Request $request)
    {
        $request->session()->forget('cart');
        return redirect('cart/cart');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $gown = Gown::FindOrFail($request->gown_id);
        $categories = Category::all();
        return view('cart/edit')->with('gown', $gown)->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gown_id = $id;
        
         if($request->session()->exists('cart.'.$gown_id)){
            $currentQuantity = $request->session()->get('cart.'.$gown_id.'.quantity');
            $totalQuantity = $request->qty + $currentQuantity;

            $request->session()->put('cart.'.$gown_id.'.quantity', "$totalQuantity");
            $request->session()->put('cart.'.$gown_id.'.date', "$request->date");
            $request->session()->put('cart.'.$gown_id.'.requirements', "$request->requirements");

        }else{
            $request->session()->put('cart.'.$gown_id.'.quantity', "$request->qty");
            $request->session()->put('cart.'.$gown_id.'.date', "$request->date");
            $request->session()->put('cart.'.$gown_id.'.requirements', "$request->requirements");          
        }
        Session::flash('success', 'Successfully Updated');
        return redirect('cart/cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function checkout(Request $request)
    {
    //     if (\Auth::check())
    //     {
    //         $cart = session()->get('cart');
    //         $gown_ids = array_keys($cart);
    //         $gown = Gown::find($gown_ids);
    
    //         $order = new Order;
    //         $order->user_id = auth()->user()->id;
    //         $order->price = $request->total;
    //         $order->save();

    //         foreach ($items as $item) 
    //         {
    //             $order_item = new OrderItem;
    //             $order_item->order_id = $order->id;
    //             $order_item->item_id = $item->id;
    //             $order_item->qty = $cart[$item->id];
    //             $order_item->save();

    //             $request->session()->forget('cart.'.$item->id);
    //         }
    //         Session::flash('success', 'Thank you for shopping!');

    //     }
    //     else
    //     {
    //         return redirect()->route('login');
    //     }  
        return view('/welcome');
     }

     public function pickup(Request $request)
    {   
        if (\Auth::check()){
            $cart = session()->get('cart');
            $gown_ids = array_keys($cart);
            $gowns = Gown::find($gown_ids);

            foreach($gowns as $gown){
                $pickup = new Pickup;
                $pickup->user_id = Auth::user()->id;
                $pickup->gown_id = $gown->id;

                $currentQuantity = session("cart.$gown->id.quantity");

                // Quantity
                $totalQuantity = $request->qty + $currentQuantity;
                // Security Fee per Gown
                $securityFee = $currentQuantity * 5000;
                // Price of all requested gown
                $price = $totalQuantity * $gown->price;
                // Price plus Security fee
                $total = $securityFee + $price;
                
                $pickup->total = $total;
                //SESION table -  cart
                $pickup->qty = session("cart.$gown->id.quantity");
                $pickup->event_date = session("cart.$gown->id.date");
                $pickup->requirements = session("cart.$gown->id.requirements");

                // FORM
                $pickup->pick_up_date = $request->pick_up_date;
                $pickup->pick_up_time = $request->pick_up_time;
                $pickup->pick_up_name = $request->pick_up_name;
                $pickup->pick_up_number = $request->pick_up_number;
                $pickup->return_date = null;
                $pickup->action_id = 1;

                $pickup->save();

                

                $request->session()->forget('cart.'.$gown->id);
            }

            // $date = new Date;
            // $date->pick_up_date_id = $pickup->pick_up_date_id;
            // $date->return_date = '';
            // $date->save();
            

            return redirect()->route('user.home');
        }else{
            return redirect()->route('login');
        }
    }

    public function editPickup($id)

    {
        $pickups = Pickup::find($id);
        return view('/cart/pickup/edit')->with('pickups', $pickups);
    }

    public function updatePickup(Request $request, $id)
    {
        
       
        // $this->validate($request, [
        //     'qty' => 'required',
        //     'event_date' => 'required',
        //     'requirements' => 'required',
        //     'pick_up_date' => 'required',
        //     'pick_up_time' => 'required',
        //     'pick_up_name' => 'required',
        //     'pick_up_phone' => 'required'


        // ]);
        
        $pickup = Pickup::find($id);

        $gowns = Gown::find($request->gown_id);


        $sum = $request->qty * $gowns->price;
        $securityFee = $request->qty * 5000;
        $pickup->user_id = auth()->user()->id;
        $pickup->gown_id = $request->gown_id;
        $pickup->qty = $request->qty;
        $pickup->total = $sum + $securityFee;
        $pickup->event_date = $request->event_date;
        $pickup->requirements = $request->requirements;
        $pickup->pick_up_date = $request->pick_up_date;
        $pickup->pick_up_time = $request->pick_up_time;
        $pickup->pick_up_name = $request->pick_up_name;
        $pickup->pick_up_number = $request->pick_up_number;
        $pickup->save();

            $pickups = DB::table('pickups')
            ->select('actions.status', 'gowns.name', 'gowns.product_code','gowns.size', 'gowns.image','gowns.price', 'users.fname','users.fname','users.email','users.number','pickups.qty', 'pickups.event_date', 'pickups.requirements','pickups.pick_up_name','pickups.pick_up_number','pickups.pick_up_time', 'pickups.pick_up_date', 'pickups.id', 'pickups.gown_id','pickups.return_date', 'pickups.total')
            ->join('actions','actions.id','=','pickups.action_id')
            ->join('gowns','gowns.id', '=','pickups.gown_id')
            ->join('users','users.id','=', 'pickups.user_id')
            ->get();

            

        
        return view('/home')->with('pickups',$pickups);
    }

    public function destroy($id)
    {
        $pickup = Pickup::find($id);
        $pickup->delete();
        return redirect('/home');
    }
}
