<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gown;
use Auth;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // if(Auth::user()->role == 'admin'){
        //     $gowns = Gown::all();
        //     $designers = Designer::all();
        //     $gownTypes = GownType::all();
        //     $categories = Category::all();
        //     $subCategories = SubCategory::all();

        //     return view('admin.index')->with('gowns',$gowns)->with('designers',$designers)->with('gownTypes', $gownTypes)->with('categories', $categories)->with('subCategories', $subCategories);
        // } else {
            $gowns = Gown::all()->random(4);
            return view('welcome')->with('gowns', $gowns);
        // }


       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
