<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Gown;
use App\Designer;
use App\GownType;
use App\Category;
use App\SubCategory;


use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // if(Auth::user()->role == 'admin'){
        //         $gowns = Gown::all();
        //         $designers = Designer::all();
        //         $gownTypes = GownType::all();
        //         $categories = Category::all();
        //         $subCategories = SubCategory::all();
    
        //         return view('admin.index')->with('gowns',$gowns)->with('designers',$designers)->with('gownTypes', $gownTypes)->with('categories', $categories)->with('subCategories', $subCategories);
        //     } else {
        //         $gowns = Gown::all()->random(4);
        //         return view('welcome')->with('gowns', $gowns);
        //     }
        
        return view('home');
    }
}
