<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gown;
use App\Designer;
use App\GownType;
use App\Category;
use App\SubCategory;
USE App\Pickup;
use App\Action;
use App\User;
use Carbon;
use Session;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gowns = Gown::all();
        $designers = Designer::all();
        $gownTypes = GownType::all();
        $categories = Category::all();
        $subCategories = SubCategory::all();
        return view('admin.index')->with('gowns', $gowns)->with('designers',$designers)->with('gownTypes', $gownTypes)->with('categories', $categories)->with('subCategories', $subCategories); 

        // $gowns = DB::table('gowns')
        //     ->select('designers.designer_name','gown_types.gown_type_name','sub_categories.sub_category_name','gowns.image', 'gowns.name','gowns.size', 'gowns.stocks','gowns.price','gowns.id','categories.category_name')
        //     ->join('gown_types','gown_types.id','=','gowns.gown_type_id')
        //     ->join('sub_categories','sub_categories.id', '=','gowns.sub_category_id')
        //     ->join('designers','designers.id','=', 'gowns.designer_id')
        //     ->join('categories','categories.id','=', 'gowns.category_id')
        //     ->get();

        // return view('admin.index')->with('gowns',$gowns);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $designers = Designer::all();
        $gownTypes = GownType::all();
        $categories = Category::all();
        $subCategories = SubCategory::all();

        return view('admin.create')->with('designers',$designers)->with('gownTypes', $gownTypes)->with('categories', $categories)->with('subCategories', $subCategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->sub_category_id);

        $gown = new Gown;

        $this->validate($request, [
            'name' => 'required',
            'product_code' => 'required',
            'stocks' => 'required',
            'size' => 'required',
            'price' => 'required',
            'description' => 'required',
            'designer_id' => 'required',
            'gown_type_id' => 'required',
            'category_id' => 'required',
            'image' => 'image'

        ]);

        // For the image uploaded, we have to make sure that it will not overwrite

        //an existing image

        //current file name
        $featured = $request->image;

        // Make a new image file name
        $featured_new_name = time().$featured->getClientOriginalName();

        // move to the uploads folder
        $featured->move('uploads/gowns/', $featured_new_name);

        $gown->name = $request->name;
        $gown->product_code = $request->product_code;
        $gown->size = $request->size;
        $gown->stocks = $request->stocks;
        $gown->price = $request->price;
        $gown->gown_type_id = $request->gown_type_id;
        $gown->description = $request->description;
        $gown->designer_id = $request->designer_id;
        $gown->category_id = $request->category_id;
        
        if($request->sub_category_id == null){
            $gown->sub_category_id = null;
        }else{
            $gown->sub_category_id = $request->sub_category_id;
        }

        $gown->image = 'uploads/gowns/'.$featured_new_name;
        $gown->save();

        Session::flash('success', 'Item have successfully added on your collection');
        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gown = Gown::find($id);
        $designers = Designer::all();
        $gownTypes = GownType::all();
        $categories = Category::all();
        $subCategories = SubCategory::all();
        return view('admin.edit')->with('gown', $gown)->with('designers',$designers)->with('gownTypes', $gownTypes)->with('categories', $categories)->with('subCategories', $subCategories);  

        // $gowns = DB::table('gowns')
        //     ->select('designers.designer_name','gown_types.gown_type_name','sub_categories.sub_category_name','gowns.image', 'gowns.name','gowns.size', 'gowns.stocks','gowns.price','gowns.id','categories.category_name')
        //     ->join('gown_types','gown_types.id','=','gowns.gown_type_id')
        //     ->join('sub_categories','sub_categories.id', '=','gowns.sub_category_id')
        //     ->join('designers','designers.id','=', 'gowns.designer_id')
        //     ->join('categories','categories.id','=', 'gowns.category_id')
        //     ->get();

        // return view('admin.edit')->with('gowns',$gowns);
       

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {      
        
        $gown = Gown::find($id);

        $this->validate($request, [
            'name' => 'required',
            'product_code' => 'required',
            'stocks' => 'required',
            'size' => 'required',
            'price' => 'required',
            'description' => 'required',
            'designer_id' => 'required',
            'gown_type_id' => 'required',
            'category_id' => 'required',
            'image' => 'image'

        ]);

        // For the image uploaded, we have to make sure that it will not overwrite

        //an existing image

        //current file name
        $featured = $request->image;

        // Make a new image file name
        $featured_new_name = time().$featured->getClientOriginalName();

        // move to the uploads folder
        $featured->move('uploads/gowns/', $featured_new_name);

        $gown->name = $request->name;
        $gown->product_code = $request->product_code;
        $gown->size = $request->size;
        $gown->stocks = $request->stocks;
        $gown->price = $request->price;
        $gown->gown_type_id = $request->gown_type_id;
        $gown->description = $request->description;
        $gown->designer_id = $request->designer_id;
        $gown->category_id = $request->category_id;
        
        if($request->sub_category_id == null){
            $gown->sub_category_id = null;
        }else{
            $gown->sub_category_id = $request->sub_category_id;
        }

        $gown->image = 'uploads/gowns/'.$featured_new_name;
        $gown->save();


        Session::flash('success', 'Item have successfully edited your collection');
   

        $designers = Designer::all();
        $gownTypes = GownType::all();
        $categories = Category::all();
        $subCategories = SubCategory::all();

        return redirect()->route('admin.index')->with('designers',$designers)->with('gownTypes', $gownTypes)->with('categories', $categories)->with('subCategories', $subCategories);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gown = Gown::find($id);
        $gown->delete();
        return redirect('admin/index');
    }

    public function dashboard()
    {
        // $designers = Designer::all();
        // $gownTypes = GownType::all();
        // $categories = Category::all();
        // $subCategories = SubCategory::all();
        // $pickups = Pickup::all();

        $pickups = DB::table('pickups')
        ->select('actions.status', 'gowns.name', 'gowns.product_code','gowns.size', 'gowns.image','gowns.price', 'users.fname','users.lname','users.email','users.number','pickups.qty', 'pickups.event_date', 'pickups.requirements','pickups.pick_up_name','pickups.pick_up_number','pickups.pick_up_time', 'pickups.pick_up_date', 'pickups.id', 'pickups.created_at', 'pickups.gown_id','pickups.return_date', 'pickups.total')
        ->join('actions','actions.id','=','pickups.action_id')
        ->join('gowns','gowns.id', '=','pickups.gown_id')
        ->join('users','users.id','=', 'pickups.user_id')
        ->get();

        return view('admin.dashboard')->with('pickups', $pickups);

        
    }


    public function indexData()
    {
        $designers = Designer::all();
        $gownTypes = GownType::all();
        $categories = Category::all();
        $subCategories = SubCategory::all();

        return view('admin.data.index')->with('designers',$designers)->with('gownTypes', $gownTypes)->with('categories', $categories)->with('subCategories', $subCategories);
    }

    public function pickup()
    {
        $pickups = DB::table('pickups')
        ->select('actions.status', 'gowns.name', 'gowns.product_code','gowns.size', 'gowns.image','gowns.price', 'users.fname','users.fname','users.email','users.number','pickups.qty', 'pickups.event_date', 'pickups.requirements','pickups.pick_up_name','pickups.pick_up_number','pickups.pick_up_time', 'pickups.pick_up_date', 'pickups.id', 'pickups.gown_id','pickups.return_date', 'pickups.total')
        ->join('actions','actions.id','=','pickups.action_id')
        ->join('gowns','gowns.id', '=','pickups.gown_id')
        ->join('users','users.id','=', 'pickups.user_id')
        ->get();

        return view('admin.pickup')->with('pickups', $pickups);

    }

    public function editPickup($id)
    {
        // $pickups = DB::table('pickups')
        // ->select('actions.status', 'gowns.name', 'gowns.product_code','gowns.size', 'gowns.image','gowns.price', 'users.fname','users.lname','users.email','users.number','pickups.qty', 'pickups.event_date', 'pickups.requirements','pickups.pick_up_name','pickups.pick_up_number','pickups.pick_up_time', 'pickups.pick_up_date', 'pickups.id', 'pickups.gown_id','pickups.return_date', 'pickups.total','pickups.id')
        // ->join('actions','actions.id','=','pickups.action_id')
        // ->join('gowns','gowns.id', '=','pickups.gown_id')
        // ->join('users','users.id','=', 'pickups.user_id')
        // ->get();
        $pickups = Pickup::find($id);
        $actions = Action::all();
        // $users = User::all();
        return view('/admin/pickup/edit')->with('pickups', $pickups)->with('actions',$actions);
        // ->with('actions',$actions)->with('users',$users);

    }

    public function updatePickup(Request $request, $id)
    {
        // dd($request);
        $this->validate($request, [
            'qty' => 'required',
            'event_date' => 'required',
            'requirements' => 'required',
            'pick_up_date' => 'required',
            'pick_up_time' => 'required',
            'pick_up_name' => 'required',
            'action_id' => 'required',
        ]);

        $gowns = Gown::find($request->gown_id);

        $pickup = Pickup::find($id);

        $sum = $request->qty * $gowns->price;
        $securityFee = $request->qty * 5000;

        $pickup->total = $sum + $securityFee;
        $pickup->qty = $request->qty;
        $pickup->event_date = $request->event_date;
        $pickup->requirements = $request->requirements;
        $pickup->pick_up_date = $request->pick_up_date;
        $pickup->pick_up_time = $request->pick_up_time;
        $pickup->pick_up_name = $request->pick_up_name;
        $pickup->pick_up_number = $request->pick_up_number;
        $pickup->action_id = $request->action_id;
        $pickup->return_date = $request->return_date;
        $pickup->save();
      
        
        $pickups = DB::table('pickups')
        ->select('actions.status', 'gowns.name', 'gowns.product_code','gowns.size', 'gowns.image','gowns.price', 'users.fname','users.lname','users.email','users.number','pickups.qty', 'pickups.event_date', 'pickups.requirements','pickups.pick_up_name','pickups.pick_up_number','pickups.pick_up_time', 'pickups.pick_up_date', 'pickups.id', 'pickups.gown_id','pickups.return_date', 'pickups.total','pickups.id')
        ->join('actions','actions.id','=','pickups.action_id')
        ->join('gowns','gowns.id', '=','pickups.gown_id')
        ->join('users','users.id','=', 'pickups.user_id')
        ->get();


        return view('admin.pickup')->with('pickups', $pickups);
    }
}
