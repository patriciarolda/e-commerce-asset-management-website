<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\Gown;
use App\Pickup;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user/about');
    }

    public function home()
    {

        $pickups = DB::table('pickups')
        ->select('actions.status', 'gowns.name', 'gowns.product_code','gowns.size', 'gowns.image','gowns.price', 'users.fname','users.fname','users.email','users.number','pickups.qty', 'pickups.event_date', 'pickups.requirements','pickups.pick_up_name','pickups.pick_up_number','pickups.pick_up_time', 'pickups.pick_up_date', 'pickups.id', 'pickups.gown_id','pickups.return_date', 'pickups.total')
        ->join('actions','actions.id','=','pickups.action_id')
        ->join('gowns','gowns.id', '=','pickups.gown_id')
        ->join('users','users.id','=', 'pickups.user_id')
        ->get();

         


        return view('home')->with('pickups', $pickups);
    }

    

    public function collection()
    {
        $categories = Category::all();
        // $category_id = Category::all();
        $subCategories = SubCategory::all();
        $gowns = Gown::all();
        return view('user/collection')->with('categories', $categories)->with('subCategories', $subCategories)->with('gowns', $gowns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gown = Gown::FindOrFail($id);
        $categories = Category::all();
        return view('user.show')->with('gown', $gown)->with('categories', $categories);
    }

    public function categorize($id)
    {
        $category_id= $id;
        $categories = Category::all();

        $gown = Gown::where('category_id', $category_id)->paginate(6);
        return view('user.show')->with('categories', $categories)->with('gown',$gown);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}
