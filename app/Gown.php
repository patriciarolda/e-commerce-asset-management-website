<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Gown extends Model
{
    public function designer()
        {
            return $this->belongsTo('App\Designer', 'designer_id');
        }

        public function category()
        {
            return $this->belongsTo('App\Category', 'category_id');
        }

        public function subcategory()
        {
            return $this->hasMany('App\SubCategory', 'sub_category_id');
        }

}
