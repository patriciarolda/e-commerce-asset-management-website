<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/user/about', 'UserController@index')->name('user.about');
Route::get('/user/collection', 'UserController@collection')->name('user.collection');
Route::get('/user/collection/{id}', 'UserController@categorize')->name('category.collection');
Route::get('/user/show/{id}', 'UserController@show')->name('user.show');
Route::get('/home', 'UserController@home')->name('user.home');


Route::get('/cart/pickup/{id}/edit', 'CartController@editPickup')->name('pickup.edit');
Route::patch('/cart/pickup/{id}', 'CartController@updatePickup')->name('pickup.update');
Route::delete('/home/{id}', 'CartController@destroy')->name('pickup.destroy');
Route::post('/cart/add/{id}', 'CartController@add')->name('cart.add');
Route::get('/cart/cart', 'CartController@index')->name('cart.all');
Route::delete('/cart/remove', 'CartController@remove')->name('cart.remove');
Route::get('/cart/empty', 'CartController@empty')->name('cart.empty');
Route::post('/cart/edit', 'CartController@edit')->name('cart.edit');
Route::post('/cart/checkout', 'CartController@checkout')->name('cart.checkout');
Route::post('/cart/pickup', 'CartController@pickup')->name('cart.pickup');


// ADMIN DA

    Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard'); 
    Route::get('/admin/index', 'AdminController@index')->name('admin.index');  
    Route::get('/admin/create', 'AdminController@create')->name('admin.create');
    Route::post('/admin/store', 'AdminController@store')->name('admin.store');
    Route::delete('/admin/destroy/{id}', 'AdminController@destroy')->name('admin.destroy');
    Route::get('/admin/{id}/edit', 'AdminController@edit')->name('admin.edit');
    Route::patch('/admin/{id}', 'AdminController@update')->name('admin.update');
    Route::get('/admin/data/index', 'AdminController@indexData')->name('admin.data.index'); 
    Route::get('/admin/pickup', 'AdminController@pickup')->name('admin.pickup');
    Route::get('/admin/pickup/{id}/edit', 'AdminController@editPickup')->name('admin.pickup.edit');
    Route::patch('/admin/pickup/{id}', 'AdminController@updatePickup')->name('admin.pickup.update');




Auth::routes();


