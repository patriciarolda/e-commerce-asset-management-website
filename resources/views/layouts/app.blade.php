<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts Style -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gotu|Lato&display=swap" rel="stylesheet">

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/f04ea524df.js" crossorigin="anonymous"></script>

    <!-- Styles: Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        {{-- Toastr --}}
        <link rel="stylesheet" href="{{ asset('css/toastr.min.css')}}">

      {{-- CSS ANIMTE --}}
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

      <link rel="stylesheet" href="{{ asset('css/style.css')}}">
</head>

<style>
    * {
        font-family: 'Gotu', sans-serif;
    }
</style>
<body>
    
    @php
        $cart_quantity = 0;
        $cart = session('cart');

        
        if (!empty($cart)) {
            $cart_quantity = count($cart);
            // $cart_quantity = array_sum('is_string', $cart) == count($cart);
            
            // $cart_quantity = array_sum($cart);
           
        } else {
            $cart_quantity = 0;
        }
    @endphp
        


    
    {{-- START: NAVBAR --}}
    <nav class="navbar navbar-expand-lg navbar-light transparent navbar-inverse" style="z-index:99;color:#000000">
        <div class="container">
            
            <a class="navbar-brand" href="{{route('welcome')}}">
                <img src="{{ URL::to('/images/navbar-logo.png') }}" alt="Mimo Couture" style="width:175px;">   
            </a>
        

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link px-3 mt-1" href="{{route('welcome')}}">Home</a>
                </li>
                <li class="nav-item">
                <a class="nav-link px-3 mt-1" href="{{route('user.about')}}">About</a>
                </li>
            
                <li class="nav-item">
                <a class="nav-link px-3 mt-1" href="{{route('user.collection')}}">Collection</a>
                </li>

                <li class="nav-item">
                <a class="nav-link px-3 mt-1" href="#">Book an appointment</a>
                </li>

                <li class="nav-item" style="border-right:1px dotted #000000;">
                <a class="nav-link px-3 mt-1" href="#">Get in touch</a>
                </li>


                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item ml-3" >
                            <a class="nav-link mt-3" href="{{ route('login') }}" style="border:1px solid;border-radius:10px;padding:5px;padding-top:1px;padding-bottom:1px;border-width:1px;border-color:#000000;color:#000000;text-decoration:none;text-transform:uppercase;font-size:10px;font-weight:bold;background:transparent;text-align:center;">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link mt-3 ml-2" href="{{ route('register') }}" style="border:1px solid;border-radius:10px;padding:5px;padding-top:1px;padding-bottom:1px;border-width:1px;border-color:#000000;color:#000000;text-decoration:none;text-transform:uppercase;font-size:10px;font-weight:bold;background:transparent;text-align:center;">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle mt-1" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Hello, {{ Auth::user()->fname }}! <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="{{ route('user.home') }}">My Closet</a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                                
                            </div>
                        </li>

                        <li class="nav-item mt-2">
                            <a href="{{ route('cart.all') }}">
                            <img src="{{ URL::to('/images/icons/cart.png') }}" alt="" style="width:25px;height:25px;"><span class="badge badge-pill" style="background-color:#EFE7E6;color:#7F7FA8;">{{ $cart_quantity}}</span>
                            </a>
                        </li>
                    @endguest
                </ul>
            </ul>
            </div>
        </div>
    </nav>
    {{-- END: NAVBAR --}}
   
    <script src="https://www.paypal.com/sdk/js?client-id=Aek_ZjKie3fBlaeWozE-Bp_xggeyH9zTuJoZn53XtERlTfxq3UmugjDvGlEJrgGHXuEcw0DSSoW1AesR&currency=PHP"></script>
    

    {{-- CONTENT --}}
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    {{-- END CONTENT --}}


    {{-- START: FOOTER --}}
    <footer style="background-color:#EFE7E6;">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-3">
                <h5>CONTACT US</h5>
                <p class="font-weight-light" style="padding-top:none;margin-buttom:0;"><small>Phone: (63)915-123-1234</small>
                <br>
                <small>Email: mimocouture@email.com</small>
                </p>
            </div>

            

            {{-- SOCIAL MEDIA --}}
            <div class="col-md-3">
                <h5>COLLECTION</h5>
                <div>
                    <a href="" class="pink-text">Wedding</a>
                </div>
                <div>
                    <a href="" class="pink-text">Debut</a>
                </div>
                <div>
                    <a href="" class="pink-text">Pageant</a>
                </div>
                <div>
                    <a href="" class="pink-text">Prom</a>
                </div>
                <div>
                    <a href="" class="pink-text">Filipiniana</a>
                </div>
                <hr>
                <h5>BY DESIGNER</h5>
                <div>
                    <a href="" class="pink-text">Yashin Lapay</a>
                </div>
                <div>
                    <a href="" class="pink-text">Ali King</a>
                </div>
                <div>
                    <a href="" class="pink-text">Bianca Aromin</a>
                </div>
                
                
            </div>

            {{-- Get latest update --}}
            <div class="col-md-3">
                <h5>MIMO COUTURE</h5>
                <div>
                    <a href="" class="pink-text">About</a>
                </div>
                <div>
                    <a href="" class="pink-text">Collection</a>
                </div>
                <div>
                    <a href="" class="pink-text">Set an Appointment</a>
                </div>
                

            </div>

            {{-- LOCATION --}}
            <div class="col-md-3">
                <h5>LOCATION</h5>
                <div class="col embed-responsive embed-responsive-16by9"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.8779056956905!2d121.05420291351548!3d14.548973882243397!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c8f2b35e2823%3A0xe5a268a1928eaa0!2sMarket%20Market!5e0!3m2!1sen!2sph!4v1586432361044!5m2!1sen!2sph" width="600" height="450" frameborder="0" style="border:0;"></iframe></div>
                <p><small>3rd floor UNIT 54, Market Market Mall, BGC, Taguig</small></p>
            </div>
        </div>
        
    </div>

    </footer>
    {{-- END: FOOTER --}}

    <script>
        // TO PROVIDE RESTRICTIONS WITH DATE INPUT
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
    
        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("datefield").setAttribute("min", today);
    
        
    </script>
    

    {{-- JS, Popper.js, and jQuery --}}
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    {{-- TOASTR JS --}}
    <script src="{{ asset('js/toastr.min.js')}}"></script>

    <script src="{{ asset('js/jquery.slim.min.js') }}" defer></script>
    <script src="{{ asset('js/popper.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>

    

</body>
</html>
