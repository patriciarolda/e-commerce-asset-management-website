<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts Style -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaina+2|Quintessential&display=swap" rel="stylesheet">

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/f04ea524df.js" crossorigin="anonymous"></script>

    <!-- Styles: Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

      {{-- Toastr --}}
      <link rel="stylesheet" href="{{ asset('css/toastr.min.css')}}">

      <link rel="stylesheet" href="{{ asset('css/style.css')}}">

      <script src="https://cdn.zingchart.com/zingchart.min.js"></script>

</head>

<style>
    * {
        font-family: 'Baloo Bhaina 2', cursive;
    }

    body {
        font-size: .875rem;
    }

    .feather {
        width: 16px;
        height: 16px;
        vertical-align: text-bottom;
    }

    /* SIDE BAR */

    .sidebar {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        z-index: 100; /* Behind the navbar */
        padding: 48px 0 0; /* Height of navbar */
        box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
    }

    .sidebar-sticky {
        position: relative;
        top: 0;
        height: calc(100vh - 48px);
        padding-top: .5rem;
        overflow-x: hidden;
        overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
        background-color:#26191b; 
    }

    @supports ((position: -webkit-sticky) or (position: sticky)) {
        .sidebar-sticky {
            position: -webkit-sticky;
            position: sticky;
        }
    }

    .sidebar .nav-link {
        font-weight: 500;
        color: #f7f7f7;
    }

    .sidebar .nav-link .feather {
        margin-right: 4px;
        color: #999;
    }

    .sidebar .nav-link.active {
        color: #007bff;
    }

    .sidebar .nav-link:hover .feather,
    .sidebar .nav-link.active .feather {
        color: inherit;
    }

    .sidebar-heading {
        font-size: .75rem;
        text-transform: uppercase;
    }

    /* CONTENT */

    [role="main"] {
        padding-top: 48px; /* Space for fixed navbar */
    }


    /* NAVBAR */

    .navbar-brand {
        padding-top: .75rem;
        padding-bottom: .75rem;
        font-size: 1rem;
        background-color: rgba(0, 0, 0, .25);
        box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);
    }

    #link-text {
        color: #fde2e2;
        
    } 



    #navName{
        font-size: 1.4em;
    }
    .navbar .form-control {
        padding: .75rem 1rem;
        border-width: 0;
        border-radius: 0;
    }

    .form-control-dark {
        color: #fff;
        background-color: rgba(255, 255, 255, .1);
        border-color: rgba(255, 255, 255, .1);
    }

    .form-control-dark:focus {
        border-color: transparent;
        box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);
    }

    img#image-hide {
        display: none;
    }   

    span:hover + img#image-hide{
        display: block;
    }
    

</style>

<body></body>
    {{-- START: SIDEBAR --}}
    <nav class="navbar navbar-light fixed-top flex-md-nowrap p-0 text-center" >
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#" style="background-color:#ea9085;font-family: 'Quintessential', cursive;">MIMO COUTURE</a>
        
        <ul class="navbar-nav px-3">
            @guest
                @else
                <li class="nav-item dropdown ml-3">
                        <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </nav>
  
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar ml-auto">
                <div class="sidebar-sticky">
                    {{-- <a class="navbar-brand" href=""> --}}
                        {{-- <img src="{{ URL::to('/images/navbar-logo.png') }}" alt="Mimo Couture" style="width:175px;" class="ml-3">    --}}
                    {{-- </a> --}}

                    {{-- START:  AUTHENTICATION --}}
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item ml-3" >
                                <a class="nav-link mt-3" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item ml-2">
                                    <a class="nav-link mt-3 ml-2" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item text-center">
                                <a id="navName" class="nav-link" href="#" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
                                    Hello {{ Auth::user()->fname }}! <span class="caret"></span>
                                </a>

                                <a  id="link-text" href="{{ route('welcome')}}" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
                                    View the sites </a> | 
                                <a  id="link-text" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                </a>
                                


                            </li>
                        @endguest
                    </ul>
                    {{-- END: AUTHENTICATION --}}

                    <hr>

                    <ul class="nav flex-column text-center">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.dashboard')}}">
                            <span data-feather="home"></span>
                            Dashboard <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.pickup')}}">
                            <span data-feather="file"></span>
                            Pick-up Requests
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                            <span data-feather="shopping-cart"></span>
                            Delivery Requests
                            </a>
                        </li>   
                    </ul>

                    

                    {{-- <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Managed Collection</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                            <span data-feather="plus-circle"></span>
                        </a>
                    </h6> --}}

                    <ul class="nav flex-column mb-2 text-center">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.index')}}">
                            <span data-feather="file-text"></span>
                            Managed Collection
                            </a>
                        </li>

                        {{-- <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.data.index')}}">
                            <span data-feather="file-text"></span>
                            Collection Data
                            </a>
                        </li> --}}

                    
                        
                        
                        <li class="nav-item ml-3">
                            <span data-feather="file-text"></span>
                            {{-- {{ date('Y-m-d H:i:s') }} --}}
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
          
        {{-- END: SIDEBAR --}}

        {{-- START: CONTENT --}}
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            
            

            {{-- MAIN CONTENT --}}
            @yield('content')

        </main>
        {{-- END: CONTENT --}}
    </div>

  
    

    
 
    {{-- JS, Popper.js, and jQuery --}}
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>    
</body>
</html>
