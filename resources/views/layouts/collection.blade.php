@extends('layouts/app')

@section('content')

<div class="container">

    <h1>GOWN RENTAL COLLECTIONS</h1>
    <div class="row">
        <div class="col-md-3">
            {{-- START: CATEGORIES --}}
            <table class="table table-hover table-sm text-center">
                <thead>
                  <tr>
                    <th scope="col">CATEGORIES</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ( $categories as $category )
                  <tr>
                    
                    <th><a href="{{route('category.collection',['id'=>$category->id])}}">{{$category->name}}</a></th>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              {{-- END: CATEGORIES --}}
        </div>
    
        <div class="col-md-9">
            @yield('collection')         
            

            
        </div>
    </div>
    

</div>

@endsection