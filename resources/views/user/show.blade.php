@extends('layouts/collection')

@section('collection')

<style>.hidden{ display: none; }</style>

<div class="container">
    <div class="card" style="border:none">
        <div class="row">

            {{-- COLUMN 1 --}}
            <div class="col-md-6">
                <img src="{{ URL::asset($gown->image) }}" alt="{{ $gown->name }}" class="img-fluid" title="{{ $gown->name}}" style="width:382.50px;height:510px;">
                
            </div>

            {{-- COLUMN 2 --}}
            <div class="col-md-6">
                <small>PID#: {{$gown->product_code}}</small> <small class="font-weight-lighter float-right">Available stock: {{$gown->stocks}}</small>
                <p class="font-weight-bold">{{$gown->name}}</p>
                <p>&#8369 {{ number_format($gown->price, 2, '.', ',') }}</p>
                <p>Available size: {{$gown->size}}</p>


                {{-- ADD TO CART FORM --}}
                <form action="{{route('cart.add',['id'=>$gown->id])}}" method="POST" >
                    @csrf
                    {{-- SIZE --}}
                    <div class="form-group mb-3 ">
                        <label for="qty">How many do you need?</label>
                        <input type="number" class="input-underline w-100" min="1" value="{{old('qty')}}" name="qty" required>
                    </div>

                    {{-- DATE --}}
                    <div class="form-group mb-3">
                        <label for="event-date">When is the event?</label>
                        <input type="date" id="datefield"  class="input-underline w-50" min="" name="date" value="{{old('date')}}" required>
                    </div>
                
                
                    {{-- ADDITIONAL REQUIREMENTS --}}
                    <div class="form-group mb-3 ">
                        <label for="requirements">Additional Requirements:</label><br>
                        <textarea name="requirements" rows="5" placeholder="Please write it here." class="w-100" required>{{old('requirements')}}</textarea>
                        
                    </div>                 

                    <div class="row text-center">
                        <div class="col-md-6">
                            <button class="btn all-button btn-sm rounded-0" type="submit">ADD TO CART</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn all-button btn-sm rounded-0" type="submit">ADD TO WISHLIST</button>
                        </div>
                    </div>                   
                </form>
                {{-- END OF ADD TO CART --}}
            </div>
        </div>
    </div>

    <div class="card mt-5" style="border:none">
        <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#must-read-tips" role="tab" aria-controls="home" aria-selected="true" style="color:#cc0e74">MUST-READ TIPS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#description" role="tab" aria-controls="profile" aria-selected="false" style="color:#cc0e74">DETAILS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="contact-tab" data-toggle="tab" href="#review" role="tab" aria-controls="contact" aria-selected="false" style="color:#cc0e74">REVIEWS</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="must-read-tips" role="tabpanel" aria-labelledby="home-tab">
                <div class="container">
                    <p style="color:#cc0e74" class="mt-3"> 1. How long does it take to receive a dress after ordering?</p>
                
                        <p>All our dresses are made-to-order. Usually orders take 4-5 weeks to process before they ship out. And it takes 3-5 working days for delivery.</p>   
    
                        <p style="color:#cc0e74">2. Can I check the color and fabric in person?</p>
                        <p>Yes! We provide color swatch service to those customers who feel uncertain about the colors. Simply click Swatches after the displayed colors, you will be directed to the color sample page.</p>
                        <p> Please note that colors may vary due to different batches of fabric, so we highly recommend ordering dresses sooner after you settle on the color.</p>
                        <p>And please keep in mind that even the same color may vary in different types of fabrics. Please make sure you are OK with the color shading if you want mix-and-match bridesmaid dresses in same color.</p>
    
                        <p style="color:#cc0e74">3. How can I avoid color variations?</p>
                        <p>Usually we suggest putting all dresses together on one order to avoid dye lot variations by using the same batch of fabric. However, it’s OK to order bridesmaid dresses separately if all orders can be placed in 2 weeks. We will collect all orders together and make sure them from same dye lot.</p>
    
                        <p style="color:#cc0e74">4. How do I know which size is right for me?</p>
                        <p>We understand the importance of sizing for such an event! So we provide both standard sizing and custom sizing service. Please follow our step by step measurement guide and compare your body measurements to the size chart and select the perfect size.</p>
                        <p>If you are in between sizes, you can just choose Custom Size (no extra cost). We will leave additional fabric in seams part in case there is any alteration.</p>
                </div>
                
            </div>
            <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="profile-tab">
                <p class="mt-3">{{$gown->description}}</p> 
            </div>
            <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="contact-tab">
                <p class="mt-3">**CURRENTLY IN PROGRESS**</p>
            </div>
          </div>

    </div>
</div>
                 





@endsection