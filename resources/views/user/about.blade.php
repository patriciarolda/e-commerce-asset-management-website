@extends('layouts/app')

@section('content')
    

<div class="container">
    <h1 class="text-center">OUR STORY</h1>
    <div class="row mt-5">
        <div class="col-md-6">
            <img src="{{ URL::to('/images/about/store2.jpeg') }}" alt="" class="img-fluid animated slideInLeft">
        </div>
        <div class=" col-md-6 mt-3 animated slideInRight">
            <h2>WE STARTED HERE</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum at rerum, voluptas delectus ut porro ab suscipit pariatur perspiciatis ex mollitia magni? Iusto veritatis pariatur nam quas dolores optio excepturi.</p>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ullam iusto sint iure. Exercitationem, consectetur? Maxime, exercitationem dolorem sapiente consectetur aspernatur quis tempore porro ipsa, rerum ea, tempora dolor. Possimus!</p>
        </div>
    </div>

    <div class="row">
        
        <div class="col-md-6 mt-3 animated slideInLeft">
            <h2>STORES</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum at rerum, voluptas delectus ut porro ab suscipit pariatur perspiciatis ex mollitia magni? Iusto veritatis pariatur nam quas dolores optio excepturi.</p>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ullam iusto sint iure. Exercitationem, consectetur? Maxime, exercitationem dolorem sapiente consectetur aspernatur quis tempore porro ipsa, rerum ea, tempora dolor. Possimus!</p>
        </div>
        <div class="col-md-6 animated slideInRight">
            <img src="{{ URL::to('/images/about/store2.jpeg') }}" alt="" class="img-fluid">
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 animated slideInLeft">
            <img src="{{ URL::to('/images/about/store3.jpeg') }}" alt="" class="img-fluid">
        </div>
        <div class="col-sm-12 col-md-6 mt-3 animated slideInRight">
            <h2>GLAMOUR!</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum at rerum, voluptas delectus ut porro ab suscipit pariatur perspiciatis ex mollitia magni? Iusto veritatis pariatur nam quas dolores optio excepturi.</p>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ullam iusto sint iure. Exercitationem, consectetur? Maxime, exercitationem dolorem sapiente consectetur aspernatur quis tempore porro ipsa, rerum ea, tempora dolor. Possimus!</p>
        </div>
    </div>

    <h1 class="text-center mt-5">IN-HOUSE DESIGNER</h1>
    <div class="row text-center">
        <div class="col-md-4">
            <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <img src="{{ URL::to('/images/about/artist1.jpeg') }}" alt="" class="rounded-circle" style="width:250px;height:250px;object-fit:cover;" >
            </a>

            <div>
                <img src="{{ URL::to('/images/icons/down-arrow.png') }}" style="width:25px;height:25px;" class="animated infinite bounce" >
            </div>

            <div class="collapse" id="collapseExample">
                <div>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                </div>
            </div>
        </div>

        

        <div class="col-md-4">
            <a data-toggle="collapse" href="#artist2" role="button" aria-expanded="false" aria-controls="collapseExample">
                <img src="{{ URL::to('/images/about/artist2.jpeg') }}" alt="" class="rounded-circle" style="width:250px;height:250px;object-fit:cover;" >
            </a>

            <div>
                <img src="{{ URL::to('/images/icons/down-arrow.png') }}" style="width:25px;height:25px;" class="animated infinite bounce" >
            </div>


            <div class="collapse" id="artist2">
                <div>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <a data-toggle="collapse" href="#artist3" role="button" aria-expanded="false" aria-controls="collapseExample">
                <img src="{{ URL::to('/images/about/artist3.jpeg') }}" alt="" class="rounded-circle" style="width:250px;height:250px" >
            </a>

            <div>
                <img src="{{ URL::to('/images/icons/down-arrow.png') }}" style="width:25px;height:25px;" class="animated infinite bounce" >
            </div>

            <div class="collapse" id="artist3">
                <div>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                </div>
            </div>
        </div>
    </div>
</div>

@endsection