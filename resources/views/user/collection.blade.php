@extends('layouts/collection')

@section('collection')
    <div class="row">
        @foreach ($gowns as $gown )
            <div class="col-md-4 mb-3 ">
                <div class="card-deck text-center">
                    <div class="card" style="border:none;   ">
                        <img src="{{ URL::asset($gown->image) }}" alt="{{ $gown->name }}" class="img-responsive" title="{{ $gown->name}}" style="width: auto; height: 20rem;" >
                        <div class="card-body ">
                            <a href="{{route('user.show',['id'=>$gown->id])}}" style="color:#ea728c"> <h5 class="card-title ">{{ $gown->name }}</a>
                        
                        <p>&#8369 {{ number_format($gown->price, 2, '.', ',') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
 
    <div class="row">
        <div class="col">
            {{-- {{ $gowns->links()}} --}}
        </div>
    </div>
@endsection
