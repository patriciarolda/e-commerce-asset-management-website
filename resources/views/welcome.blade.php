@extends('layouts/app')

@section('content')
    {{-- START:CAROUSEL --}}
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top:-150px;">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ URL::to('/images/carousel-gown/5.jpeg') }}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{ URL::to('/images/carousel-gown/3.jpg') }}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{ URL::to('/images/carousel-gown/4.jpg') }}" class="d-block w-100" alt="...">
            </div>
        </div>

        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>
    {{-- END: CAROUSEL --}}


    {{-- START: HOME SERVICE WORK --}}
    <div class="container">
        <h3 class="text-center mt-5 pb-2 mb-3 border-bottom" style="font-family: 'Gotu', sans-serif;">HOME SERVICES BY MIMO COUTURE</h3>
        <div class="row mt-5">
            
            <div class="col-md-4 text-center border-right">
                <img src="{{ URL::to('/images/icons/gown.png') }}" alt="" class="img-responsive mb-2" style="widht:100px;height:100px">
                <p class="home-service">BROWSE THE COLLECTION</p>
                <p>Choose from our inventory of over 5,000 fashionable and unique items. New arrivals are added weekly!</p>
            </div>
            <div class="col-md-4 text-center border-right">
                <img src="{{ URL::to('/images/icons/form.png') }}" alt="" class="img-responsive mb-2" style="widht:100px;height:100px">
                <p class="home-service">FILL UP ONLINE FORM</p>
                <p>Complete the details required for a personalized service custom fit to your needs.</p>
            </div>
            <div class="col-md-4 text-center">
                <img src="{{ URL::to('/images/icons/deliver.png') }}" alt="" class="img-responsive mb-2" style="widht:100px;height:100px">
                <p class="home-service">WE DELIVER AND PICK UP</p>
                <p>Drop off at the store or return by delivery. We dry clean and quality inspect every item, all you have to worry about is enjoying your event.</p>
            </div>
        </div>
    </div>
    {{-- END: HOME SERVICE WORK --}}

    {{-- START: SERVICES OFFERED --}}
    <div class="container">
        <h3 class="text-center mt-5 pb-2 mb-3 border-bottom" style="font-family: 'Gotu', sans-serif;">ALL SERVICES OFFERED</h3>
        <div class="row text-center mt-5">
            <div class="col-md-3">
                <img src="{{ URL::to('/images/icons/check.png') }}" alt="" class="img-responsive mb-2" style="widht:50px;height:50px">
                <p>READY TO WEAR</p>
            </div>
            <div class="col-md-3">
                <img src="{{ URL::to('/images/icons/first.png') }}" alt="" class="img-responsive mb-2" style="widht:50px;height:50px">
                <p>FRIST USE RENT</p>
            </div>
            <div class="col-md-3">
                <img src="{{ URL::to('/images/icons/dry-cleaning.png') }}" alt="" class="img-responsive mb-2" style="widht:50px;height:50px">
                <p>DRY CLEANING & PRESS</p>
            </div>
            <div class="col-md-3">
                <img src="{{ URL::to('/images/icons/scissors.png') }}" alt="" class="img-responsive mb-2" style="widht:50px;height:50px">
                <p>ALTERATION</p>
            </div>
        </div>
    </div>
    {{-- END: SERVICES OFFICER --}}

    {{-- START: COLLECTION --}}
    <div class="container mt-5">
        <h3 class="text-center my-5 pb-2 mb-3 border-bottom" style="font-family: 'Gotu', sans-serif;">COLLECTIONS</h3>

        <div class="row">
            @foreach ($gowns as $gown )
                <div class="col-md-3 mb-3 ">
                    <div class="card-deck text-center">
                        <div class="card" style="border:none;   ">
                            <img src="{{ URL::asset($gown->image) }}" alt="{{ $gown->name }}" class="img-responsive" title="{{ $gown->name}}" style="width: auto; height: 20rem;" >
                            <div class="card-body ">
                                <a href="{{route('user.show',['id'=>$gown->id])}}" style="color:#ea728c"> <h5 class="card-title ">{{ $gown->name }}</a>
                            
                            <p>&#8369 {{ number_format($gown->price, 2, '.', ',') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    {{-- END: COLLECTION --}}
@endsection