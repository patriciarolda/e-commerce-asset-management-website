@extends('layouts.app')

@section('content')

<div class="container">

    

        <h2>My Collections</h2>
    {{-- @if (!empty($pickups->user_id)) --}}
    <nav>
        
        <div class="nav nav-tabs justify-content-end" id="nav-tab" role="tablist">
        
        
          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pick-up</a>
          <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Delivery</a>
    
        </div>
        
    </nav>
      <div class="tab-content" id="nav-tabContent">
        {{-- PICK-UP --}}
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <div class="container">
               
                <div class="row">
                     
                        <div class="col">
                            @foreach ($pickups as $pickup)
                           
                                <div class="card my-2  rounded-0  px-2 py-2 border-top-0 border-right-0 border-left-0">
                                    <div class="container">

                                     
                                    <h4 class="h-25">
                                        <a href="{{route('user.show',['id'=>$pickup->gown_id])}}" class="pink-text text-decoration-none"> 
                                        {{$pickup->name}}
                                        </a>
                                    </h4>
                                      
                                        
                                        <div class="row">
                                            <div class="col-md-3" style="padding-right:0;">
                                                <img src="{{ URL::asset($pickup->image) }}" alt="{{ $pickup->name }}" class="img-fluid" title="{{ $pickup->name}}" style="width:216px;height:287px;">
                                            </div>
                
                                            <div class="col-md-3">
                                                <h6 class="font-weight-bolder">Order Details:</h6>

                                                <div class=" font-weight-light"><small><strong>Gown Rental Fee:</strong> &#8369 {{ number_format($pickup->price, 2, '.', ',') }}</small></div>

                                                <div class=" font-weight-light"><small><strong>Size:</strong> {{$pickup->size}}</small></div>
                                                
                                                <div class=" font-weight-light"><small><strong>Quantity:</strong> {{$pickup->qty}}</small></div>
                
                                                <div class=" font-weight-light"><small><strong>Event Date:</strong> {{$pickup->event_date}}</small></div>
                
                                                 <div class=" font-weight-light text-justify"><small><strong>Additional Requirements: </strong><br>{{$pickup->requirements}}</small></div>
                
                                                
                                            </div>
               
                                            <div class="col-md-3">
                                                <h6 class="font-weight-bolder">Pick-up Information</h6>
                                                <div class=" font-weight-light"><small><strong>Name:</strong> {{$pickup->pick_up_name}}</small></div>
                 
                                                <div class=" font-weight-light"><small><strong>Contact number:</strong> {{$pickup->pick_up_number}}</small></div>
                
                                                <div class=" font-weight-light"><small><strong>Pic-up Date:</strong> {{$pickup->pick_up_date}}</small></div>
                
                                                <div class=" font-weight-light"><small><strong>Pick-up Time:</strong> {{$pickup->pick_up_time}}</small></div>

                                                <hr>
                                                <h6 class="font-weight-bolder">Total Amount</h6>
                                                <table class="table table-sm">       
                                                    
                                                    @php

                                                        $security = $pickup->qty * 5000;

                                                        $totalRental = $pickup->price * $pickup->qty;
                                                        
                                                        

                                                    @endphp

                                                    <tbody>
                                                        <tr>
                                                            <td style="border-top:none;"><small>Rental Fee</small></td>
                                                            <td style="border-top:none;"><small>&#8369 {{ number_format($totalRental, 2, '.', ',') }}</small></td>      
                                                        </tr>

                                                        <tr>
                                                            <td style="border-top:none;"><small>Security Fee</small></td>
                                                            <td style="border-top:none;"><small>&#8369 {{ number_format($security, 2, '.', ',') }}</small></td>      
                                                        </tr>

                                                        <tr>
                                                            <td class="font-weight-bolder" style="border-top:none;"><small>TOTAL</small></td>
                                                            <td class="font-weight-bolder" style="border-top:none;"><small>&#8369 {{ number_format($pickup->total, 2, '.', ',') }}</small></td>      
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <div class=" font-weight-light"><small>***We accept cash and credit card.</small></div>
                                                

                                            </div>

                                            <div class="col-md-3">
                                                @if ($pickup->id == 1)
                                                    <h6><span class="badge" style="background-color:#9d0b0b;">
                                                    Status: {{$pickup->status}}
                                                    </span></h6>
                                                @endif
                                                @if($pickup->id == 2)
                                                    <h6><span class="badge" style="background-color:#cd8d7b;">
                                                        Status: {{$pickup->status}}
                                                    </span></h6>
                                                @endif
                                                @if($pickup->id == 3)
                                                    <h6><span class="badge" style="background-color:#000000;color:white">
                                                        Status: {{$pickup->status}}
                                                    </span></h6>
                                                @endif
                                                @if($pickup->id == 4)
                                                    <h6><span class="badge" style="background-color:#7f78d2;color:white;">
                                                        Status: {{$pickup->status}}
                                                    </span></h6>
                                                @endif
                                                @if($pickup->id == 5)
                                                    <h6><span class="badge" style="background-color:#c81912;color:white;">
                                                        Status: {{$pickup->status}}
                                                    </span></h6>
                                                @endif
                                                @if($pickup->id == 6)
                                                    <h6><span class="badge" style="background-color:#9d0b0b;color:white;">
                                                        Status: {{$pickup->status}}
                                                    </span></h6>
                                                @endif
                                                @if($pickup->id == 7)
                                                    <h6><span class="badge" style="background-color:#e25822;">
                                                        Status: {{$pickup->status}}
                                                    </span></h6>
                                                @endif

                                                

                                                

                                                
                                                
                                                @if(empty($pickup->return_date))
                                                <div class=" font-weight-light"><small><strong>Return Date:</strong> <br>Once approved by Mimo Couture, date will be posted.</small></div>
                                                @else

                                                <h6><span class="badge" style="background-color:#EF8D8C;">
                                                Return Date: {{$pickup->return_date}}
                                                </span></h6>
                                                @endif
                                                

                                            </div>
                                        </div>
                
                                        <form method="post" action="{{route('pickup.destroy',['id'=>$pickup->id])}}" class="float-right">
                                            @csrf
                                            @method('DELETE')
                                            <input type='hidden' name='gown_id' value='{{$pickup->id}}'>
                                            <button type="submit" class="btn btn-white" href=""><img src="{{ URL::to('/images/icons/delete-gray.png') }}" alt="Cancel Reservation" style="width:20px;" title="Delete Item"> </button>
                                        </form>

                                        
                                            <input type="hidden" name="gown_id" value=''>

                                            <a href="{{route('pickup.edit',['id'=>$pickup->id])}}"  class="btn btn-white float-right">
                                                <img src="{{ URL::to('/images/icons/edit-gray.png') }}" alt="Edit Information" style="width:20px;" class="text-right" title="Edit Information">
                                            </a>
                                        
                                                                            
                                            
                
                                    </div>
                                </div>
                            @endforeach
                         </div>
                     
                     
                         
                     {{-- <div class="col-md-4">
                         <div class="card my-2  rounded-0  px-2 py-2">
                         
                             TEST
                         </div>
                     </div> --}}
                     
                 </div>
             </div>
        </div>

        {{-- DELIVERY --}}
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
    
      </div>

      {{-- @else
      <div class="container vh-100">
          <p>You have no items in your collection.</p>
          <a class="h6 pink-text" href="{{route('user.collection')}}">Click here to shop.</a>
    @endif --}}
</div>







@endsection
