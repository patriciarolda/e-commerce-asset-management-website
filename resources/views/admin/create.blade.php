@extends('layouts/admin')

@section('content')

    @can('isAdmin')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"> <img src="{{ URL::to('/images/icons/gown.png') }}" alt="" class="img-responsive mb-2" style="widht:35px;height:35px"> ADD COLLECTION</h1>
    </div>

    <div class="container px-5 ">
        <form method="POST" action="{{route('admin.store')}}" enctype="multipart/form-data">
            @csrf   

            <div class="form-group">
                <label for="name">Product ID: </label>
                <input type="text" class="form-control" min="21" name="product_code" value="{{old('product_code')}}" >
                @error('product_code')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>


            <div class="form-group">
                <label for="name">Name: </label>
                <input type="text" class="form-control" min="21" name="name" value="{{old('name')}}" >
                @error('name')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="name">Size: </label>
                <input type="text" class="form-control" name="size" value="{{old('size')}}" >
                @error('size')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="price">Quantity</label>
                <input type="number" class="form-control" name="stocks" value="{{old('stocks')}}" min="1">
                @error('stocks')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>
    
            <div class="form-group">
                <label for="price">Price </label>
                <input type="number" class="form-control" name="price" value="{{old('price')}}" >
                @error('price')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>
    
            <div class="form-group">
                <label for="category">Designer</label>
                <select name="designer_id" class="form-control" >
                <option name="designer_id" value="{{old('designer_id')}}" disabled selected>Select Designer</option>
                    @foreach($designers as $designer)
                <option name="designer_id" value="{{$designer->id}}">{{$designer->name}}</option>
                    @endforeach
                </select>
                @error('designer')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>
    
            <div class="form-group">
                <label for="category">Type of Gown</label>
                <select name="gown_type_id" class="form-control" >
                <option name="gown_type_id" value="{{old('gown_type_id')}}" disabled selected>Select Type of Gown</option>
                    @foreach($gownTypes as $gownType)
                <option name="gown_type_id" value="{{$gownType->id}}">{{$gownType->name}}</option>
                    @endforeach
                </select>
                @error('gown')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>
    
            <div class="form-group">
                <label for="category">Categories</label>
            
                <select name="category_id" class="form-control" required>
                <option name="category_id" value="{{old('category_id')}}" disabled selected>Select Categories</option>
                    @foreach($categories as $category)
                <option  name="category_id" value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
    
                (Kindly check if with Sub-category) <input type="checkbox" id="terms_and_conditions" onclick="subcategory(this)">
                <select name="sub_category_id" class="form-control" id="subCat"  disabled>
                <option name="sub_category_id" value="{{old('sub_category_id')}}" disabled selected>Select Sub-categories</option>
                    @foreach($subCategories as $subCategory)
                <option name="sub_category_id" value="{{$subCategory->id}}">{{$subCategory->name}}</option>
                    @endforeach
                </select>

                @error('categories')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>
    
    
            <div class="form-group">
                <label for="name">Description </label>
                <textarea class="form-control" rows="3" name="description" >{{old('description')}}</textarea>
                @error('description')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>
    
            <div class="form-group">
                {{-- <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image" name="image" >
                    <label class="custom-file-label">Choose file</label>
                </div> --}}

                
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control">
                
                @error('image')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror
            </div>
        
            <button type="submit" class="btn btn-outline-dark my-3">Submit</button>   
        </form>
    </div>
    
    
    <script>
        function subcategory(subCheckBox){     
            if(subCheckBox.checked){        
                document.getElementById("subCat").disabled = false;
            } else{
                document.getElementById("subCat").disabled = true;
            }
        }
    </script>

    @endcan

@endsection