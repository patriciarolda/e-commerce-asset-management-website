@extends('layouts/admin')

@section('content')

<div class="container">

  <div class="card">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" style="color:#cc0e74">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" style="color:#cc0e74">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false" style="color:#cc0e74">Contact</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">...</div>
      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
    </div>
  </div>

    <div class="top">
      <h3 class="mt-5">CATEGORIES <a href="{{ route('admin.create') }}" type="button" class="align-middle"><img src="{{ URL::to('/images/icons/add2.png') }}" alt="" class="img-responsive" style="widht:20px;height:20px" title="Add Category"></a></h3>

     
    </div>
    

    <div class="card rounded-0" style="border:2px solid #EF8D8C;">
        <div class="card-body">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col" style="border-top:none;"></th>
                    <th scope="col" style="border-top:none;">Category Name</th>
                    <th scope="col" class="float-right" style="border-top:none;">Action</th>
                  </tr>
                </thead>
                <tbody>

                @foreach ($categories as $category)
                    
                
                  <tr>
                    <th scope="row"><input type="checkbox"></th>
                    <td>{{ $category->name}}</td>
                    <td>
                        <div class="text-right">
                            <form method="post" action="" class="float-right">
                              @csrf
                              @method('DELETE')
                              <input type='hidden' name='' value=''>
                              <button type="submit" class="btn btn-white" href=""><img src="{{ URL::to('/images/icons/delete-gray.png') }}" alt="Delete" style="width:20px;" title="Delete Item"> </button>
                            </form>
                                            
                            <form method="post" action="" class="float-right">
                                @csrf
                                <input type="hidden" name="gown_id" value=''>
                                <button type="submit" class="btn btn-white">
                                    <img src="{{ URL::to('/images/icons/edit-gray.png') }}" alt="Edit Cart" style="width:20px;" class="text-right" title="Edit Cart">
                                </button>
                            </form>
                            
                                                                  
                               
                             </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>

@endsection