@extends('layouts/admin')

@section('content')

    <div class="container">

        


    
        <hr>
        @foreach ($pickups as $pickup)
        <div class="card my-2  rounded-0  px-2 py-2 border-top-0 border-right-0 border-left-0">
            <div class="container">
                
                @for ($i =0 ; $i < $pickup->id ; $i++ )
                    <h4 class="h-25">Pick-up Request</h4>
                    <h1> {{$i+=1}}</h1>
                @endfor
                
       
                 
                <div class="row">
                    <div class="col-md-3" style="padding-right:0;">
                        <h6 class="font-weight-bolder">User Details:</h6>
                        <div class=" font-weight-light"><small><strong>Name:</strong> {{$pickup->fname}} </small></div>
                        <div class=" font-weight-light"><small><strong>Phone Number:</strong> {{$pickup->number}}</small></div>
                        <div class=" font-weight-light"><small><strong>Email:</strong> {{$pickup->email}}</small></div>
                        

                       
                    </div>

                     <div class="col-md-3">
                        <h6 class="font-weight-bolder">Order Details:</h6>
                        <div class=" font-weight-light"><small><strong>Product ID #</strong> {{$pickup->product_code}}</small></div>

                        <div class=" font-weight-light"><small><strong>Gown:</strong> {{$pickup->name}}</small></div>

                        <div class=" font-weight-light"><small><strong>Size:</strong> {{$pickup->size}}</small></div>
                        
                        <div class=" font-weight-light"><small><strong>Quantity:</strong> {{$pickup->qty}}</small></div>

                        <div class=" font-weight-light"><small><strong>Event Date:</strong> {{$pickup->event_date}}</small></div>

                        <div class=" font-weight-light text-justify"><small><strong>Additional Requirements: </strong><br>{{$pickup->requirements}}</small></div>

                         
                    </div>

                    <div class="col-md-3">
                        <h6 class="font-weight-bolder">Pick-up Information</h6>
                        <div class=" font-weight-light"><small><strong>Name:</strong> {{$pickup->pick_up_name}}</small></div>

                        <div class=" font-weight-light"><small><strong>Contact number:</strong> {{$pickup->pick_up_number}}</small></div>

                        <div class=" font-weight-light"><small><strong>Pic-up Date:</strong> {{$pickup->pick_up_date}}</small></div>

                        <div class=" font-weight-light"><small><strong>Pick-up Time:</strong> {{$pickup->pick_up_time}}</small></div>
                    </div> 

                     <div class="col-md-3">
                       
                        
                            <h6><span class="badge" style="background-color:#e25822;">
                                Status: {{$pickup->status}}
                            </span></h6>
                            
                        
                        @if(empty($pickup->return_date))
                        <div class=" font-weight-light"><small><strong>Return Date:</strong> <br>Please set a return date.</small></div>
                        @else

                        <h6><span class="badge" style="background-color:#EF8D8C;">
                        Return Date: {{$pickup->return_date}}
                        </span></h6>
                        @endif
                        
                    </div>
                </div>

                <form method="post" action="{{route('pickup.destroy',['id'=>$pickup->id])}}}}" class="float-right">
                    @csrf
                    @method('DELETE')
                    <input type='hidden' name='gown_id' value='{{$pickup->id}}'>
                    <button type="submit" class="btn btn-white" href=""><img src="{{ URL::to('/images/icons/delete-gray.png') }}" alt="Cancel Reservation" style="width:20px;" title="Delete Item"> </button>
                </form> 

                <form method="get" action="{{route('admin.pickup.edit',['id'=>$pickup->id])}}" class="float-right">
            
                    
                    <button type="submit" class="btn btn-white">
                        <img src="{{ URL::to('/images/icons/edit-gray.png') }}" alt="Edit Information" style="width:20px;" class="text-right" title="Edit Information">
                    </button>
                </form>
                
                                                      
                    

            </div>
        </div>
        @endforeach

    </div>

@endsection