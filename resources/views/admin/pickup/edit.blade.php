@extends('layouts/admin')

@section('content')

    <div class="container">
        <h5 class="mt-3">Admin's Approval</h5>
        <hr>
        {{-- @foreach ($pickups as $pickup) --}}
        <div class="card my-2  rounded-0  px-2 py-2 border-top-0 border-right-0 border-left-0">

        <form method="POST" action="{{ route('admin.pickup.update', ['id' => $pickups->id]) }}" class="float-right">
        @csrf 
        @method('PATCH')
            <div class="container">
                <h4 class="h-25">Pick-up Request #</h4>

                <div class="row">
                    <div class="col-md-3" style="padding-right:0;">
                        <h6 class="font-weight-bolder">User Details:</h6>
                        
                        <div class=" font-weight-light"><small><strong>Name:</strong> {{$pickups->fname}} {{$pickups->lname}}</small></div>
                        
                        <div class=" font-weight-light"><small><strong>Phone Number:</strong> {{$pickups->number}}</small></div>
                        
                        <div class=" font-weight-light"><small><strong>Email:</strong> {{$pickups->email}}</small></div>

                       
                    </div>

                    <div class="col-md-3" style="padding:0 0;">
                        <h6 class="font-weight-bolder">Order Details:</h6>
                        <div class=" font-weight-light"><small><strong>Product ID #</strong> {{$pickups->product_code}}</small></div>

                        <div class=" font-weight-light"><small><strong>Gown:</strong> {{$pickups->name}}</small></div>

                        <div class=" font-weight-light"><small><strong>Size:</strong> {{$pickups->size}}</small></div>
                        
                        <div class=" font-weight-light "><small><strong>Quantity:</strong></small>
                            <input type="number" class="input-underline w-50" min="1" value="{{$pickups->qty}}" name="qty">
                        </div>
                        

                        <div class=" font-weight-light"><small><strong>Event Date:
                            <input type="date" id="datefield"  class="input-underline w-50" min="" name="event_date" value="{{$pickups->event_date}}">
                        </strong></small></div>

                        <div class=" font-weight-light text-justify"><small><strong>Additional Requirements: </strong><br>
                            <textarea name="requirements" rows="5" placeholder="Please write it here." class="w-100">{{$pickups->requirements}}</textarea></small>
                        </div>

                         
                    </div>

                    <div class="col-md-3">
                        <h6 class="font-weight-bolder">Pick-up Information</h6>
                        <div class=" font-weight-light"><small><strong>Name:</strong>
                            <input type="text" class="input-underline w-50" min="" name="pick_up_name" value="{{$pickups->pick_up_name}}" >
                        </small></div>

                        <div class=" font-weight-light"><small><strong>Contact number:
                            <input type="number" class="input-underline w-50 mb-4" name="pick_up_number" value="{{$pickups->pick_up_number}}">
                        </strong></small></div>

                        <div class=" font-weight-light"><small><strong>Pic-up Date:</strong> </small>
                            <input type="date" id="datefield"   class="input-underline w-50" min="" name="pick_up_date" value="{{$pickups->pick_up_date}}">
                        </div>

                        <div class=" font-weight-light"><small><strong>Pick-up Time:</strong> </small>
                            <select class="input-underline w-50" id="time" name="pick_up_time">
                                <option name="pick_up_time" value="{{$pickups->pick_up_time}}" {{ $pickups->pick_up_time == $pickups->pick_up_time ? 'selected' : '' }}>{{$pickups->pick_up_time    }}</option>
                                <option value="10:00am">10:00am</option>
                                <option value="11:00am">11:00am</option>
                                <option value="1:00pm">1:00pm</option>
                                <option value="2:00pm">2:00pm</option>
                                <option value="3:00pm">3:00pm</option>
                                <option value="4:00pm">4:00pm</option>
                                <option value="5:00pm">5:00pm</option>
                                <option value="6:00pm">6:00pm</option>
                                <option value="7:00pm">7:00pm</option>
                                <option value="8:00pm">8:00pm</option>
                                </select>
                        </div>
                    </div>

                    <div class="col-md-3">

                        <div class="form-group badge w-100" style="background-color:#EF8D8C;">
                            <label for="category">Status:</label>
                            <select name="action_id" class="input-underline w-" >
                            {{-- <option name="action_id" value="{{$actions->id}}" {{ $actions->id == $actions->id ? 'selected' : '' }}>{{$actions->status    }}</option> --}}
                                @foreach($actions as $action)
                            <option  value="{{$action->id}}">{{$action->status}}</option>
                                @endforeach
                            </select>
                        </div>
                            
                        <div class=" form-group w-100">
                            <label for="category">Scheduled Return Date:</label>
                            <input type="date" id="datefield"   class="input-underline w-100" min="" name="return_date" value="{{$pickups->return_date}}">
                        </div>  
                    </div>
                </div> 
                

                <input type="hidden" name="gown_id" value="{{$pickups->gown_id}}">
      


                <div class="text-right">
                    <button type="submit" class="btn all-button rounded-0 w-25 mt-3">Update</button>
                </div>
                
                
        </form>
                
                                                      
                

            </div>
        </div>
    </div>

    <script>
        // TO PROVIDE RESTRICTIONS WITH DATE INPUT
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
    
        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("datefield").setAttribute("min", today);
    
        
    </script>

@endsection