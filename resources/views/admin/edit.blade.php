@extends('layouts/admin')

@section('content')

    @can('isAdmin')
    {{-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        
    </div> --}}

    <div class="container px-5 ">
        <div class="card px-5 py-5 mb-5">
            <h1>  EDIT COLLECTION</h1>
            <form method="post" action="{{route('admin.update',['id'=>$gown->id])}}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Name: </label>
                    <input type="text" class="form-control input-underline" name="name" value="{{ $gown->name }}" >
                    @error('name')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                </div>
        
                <div class="form-group">
                    <label for="price">Price </label>
                    <input type="number" class="form-control input-underline" name="price" value="{{ $gown->price }}" >
                    @error('price')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                </div>
        
                <div class="form-group">
                    <label for="category">Designer</label>
                    <select name="designer_id" class="form-control input-underline" >
                    <option name="designer_id" disabled selected>Select Designer</option>
                        @foreach($designers as $designer)
                    <option name="designer_id" value="{{$designer->id}}" {{ $designer->id == $designer->id ? 'selected' : '' }}>{{$designer->designer_name}}</option>
                        @endforeach
                    </select>
                    @error('designer_id')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                </div>
        
                <div class="form-group">
                    <label for="category">Type of Gown</label>
                    <select name="gown_type_id" class="form-control input-underline" >
                    <option name="gown_type_id"  disabled selected>Select Type of Gown</option>
                        @foreach($gownTypes as $gownType)
                    <option name="gown_type_id" value="{{$gownType->id}}" {{ $gownType->id == $gownType->id ? 'selected' : '' }}>{{$gownType->gown_type_name}}</option>
                        @endforeach
                    </select>
                    @error('gown_type_id')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                </div>
        
                <div class="form-group">
                    <label for="category">Categories</label>
                
                    <select name="category_id" class="form-control input-underline" required>
                    <option name="category_id"  disabled selected>Select Categories</option>
                        @foreach($categories as $category)
                    <option  name="category_id" value="{{$category->id}}" {{ $category->id == $category->id ? 'selected' : '' }}>{{$category->category_name}}</option>
                        @endforeach
                    </select>
        
                    (Kindly check if with Sub-category) <input type="checkbox" id="terms_and_conditions" onclick="subcategory(this)">
                    <select name="sub_category_id" class="form-control input-underline" id="subCat"  disabled>
                    <option name="sub_category_id"  disabled selected>Select Sub-categories</option>
                        @foreach($subCategories as $subCategory)
                    <option name="sub_category_id" value="{{$subCategory->id}}" {{ $subCategory->id == $subCategory->id ? 'selected' : '' }}>{{$subCategory->sub_category_name}}</option>
                        @endforeach
                    </select>
    
                    @error('category_id')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                </div>
        
        
               <div class="form-group">
                    <label for="name">Description  </label>
                    <textarea class="form-control input-underline" rows="10" name="description" >{{ $gown->description}}</textarea>
                    @error('description')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                </div>
        
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input " id="image" name="image"  value="{{ $gown->image }}">
                        {{-- <input type="file" name="image" class="form-control" value="{{ $gown->image }}"> --}}
                        <label class="custom-file-label" for="inputGroupFile04">Choose file: {{ $gown->image }}</label>
                    </div>
                    @error('image')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                </div>
            
                <button type="submit" class="btn all-button  my-3 ">Update</button>   
            </form>
        </div>
        
    </div>
    
    
    <script>
        function subcategory(subCheckBox){     
            if(subCheckBox.checked){        
                document.getElementById("subCat").disabled = false;
            } else{
                document.getElementById("subCat").disabled = true;
            }
        }
    </script>

    @endcan

@endsection