@extends('layouts/admin')

@section('content')

    @can('isAdmin')
    <div class="container">
        
          
        <div class="row nav-pills "  id="pills-tab">
            <div class="col-md-3">
                <div class="card bg-light mb-3 border-right-0 border-left-0 border-bottom-0" style="max-width: 18rem;border-color:#363062;">
                    <div class="card-body">
                        <img src="{{ URL::to('/images/icons/pickup.png') }}" alt="Pick-up" style="width:60px;" class="text-center" title="Pick-up">
                      <p class="text-center font-weight-bold float-right">PICK-UP REQUEST</p>
                    </div>
                  </div>
            </div>

            <div class="col-md-3">
                <div class="card bg-light mb-3 border-right-0 border-left-0 border-bottom-0" style="max-width: 18rem;border-color:#363062;">
                    <div class="card-body">
                        <img src="{{ URL::to('/images/icons/delivery-logo.png') }}" alt="Pick-up" style="width:60px;" class="text-center" title="Pick-up">
                      <p class="text-center font-weight-bold float-right">DELIVER REQUEST</p>
                    </div>
                  </div>
            </div>

            <div class="col-md-3">
                <div class="card bg-light mb-3 border-right-0 border-left-0 border-bottom-0" style="max-width: 18rem;border-color:#363062;">
                    <div class="card-body">
                        <img src="{{ URL::to('/images/icons/return.png') }}" alt="Pick-up" style="width:65px;" class="text-center" title="Pick-up">
                      <p class="text-center font-weight-bold float-right">RETURNED</p>
                    </div>
                  </div>
            </div>

            <div class="col-md-3">
                <div class="card bg-light mb-3 border-right-0 border-left-0 border-bottom-0" style="max-width: 18rem;border-color:#363062;">
                    <div class="card-body">
                        <img src="{{ URL::to('/images/icons/revenue.png') }}" alt="Pick-up" style="width:65px;" class="text-center" title="Pick-up">
                      <p class="text-center font-weight-bold float-right">REVENUE</p>
                    </div>
                  </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-6">
                <table class="table rounded table-sm" >
                    <thead>
                    
                      <tr>
                        
                        <th scope="col">User</th>
                        
                        <th scope="col">Date Requested</th>
                        <th scope="col">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($pickups as $pickup )
                        <tr>
                            
                            <td>{{$pickup->fname}} {{$pickup->lname}}</td>
                            
                            <td>{{$pickup->created_at}}
                                {{-- {{$pickup->created_at->format('Y-m-d')}}</td> --}}
                            <td id="status">{{$pickup->status}}</td>
                        </tr>
                        @endforeach
                      

                    </tbody>
                  </table>
            </div>

            

            <hr>

            <div class="col-md-6">
                <div class="container" id="myChart">

                </div>
            </div>

            
        </div>
        
    </div>

    @endcan

    <script>
        var status = document.getElementById("status")
        var chartData = {
          type: 'pie',
          series: [
            { values: [2] },
            { values: [0] },
            { values: [0] }
          ]
        };
        zingchart.render({
          id: 'myChart',
          data: chartData,
          height: 400,
          width: 400
        });
      </script>

      {{-- <script>
          var analytics = {{ $status }}

        google.charts.load('current', {'packages':['corechart']});

        google.charts.setOnLoadCallback(drawChart);

        function drawChart()
        {
        var data = google.visualization.arrayToDataTable(analytics);
        var options = {
        title : 'Percentage of Male and Female Employee'
        };
        var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
        chart.draw(data, options);
        }
      </script> --}}

@endsection