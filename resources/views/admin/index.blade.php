@extends('layouts/admin')

@section('content')

    @can('isAdmin')

    {{-- <div class="card"> --}}
        <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
            {{-- COLLECTIONS --}}
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#collections" role="tab" aria-controls="home" aria-selected="true">Collections</a>
            </li>

            <li class="nav-item">
                <a class="nav-link pink-text" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Managed Categories</a>
            </li>


        </ul>


        <div class="tab-content" id="myTabContent">
            {{-- COLLECTIONS --}}
            <div class="tab-pane fade show active" id="collections" role="tabpanel" aria-labelledby="home-tab">
                <div class="container-fluid">
                    
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
                        {{-- FILTER BY CATEGORY --}}
                        <div class="input-group w-25  mr-auto mb-1">
                            <select class="custom-select " id="inputGroupSelect04" aria-label="Example select with button addon">
                            <option selected disabled>Filter by Category</option>
                            @foreach ($categories as $category )
                                <option value="{{ $category->id}}">{{ $category->category_name}}</option>
                            @endforeach
                            
                            </select>
                            <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button"><i class="fas fa-caret-down"></i></button>
                            </div>
                        </div>
                
                      
                        {{-- SEARCH --}}
                        <form class="form-inline d-flex md-form form-sm mr-3">
                            <div class="input-group md-form form-sm form-2 pl-0">
                                <input class="form-control my-0 py-1 red-border" type="text" placeholder="Search" aria-label="Search">
                                <div class="input-group-append">
                                    <span class="input-group-text red lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                                        aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                        
                    <table class="table ">
                        <thead>
                            <tr>
                                {{-- <th scope="col">#</th> --}}
                                <th scope="col" class="border-top-0"></th>
                                <th scope="col" class="border-top-0">Name</th>
                                <th scope="col" class="border-top-0">Quantity</th>
                                <th scope="col" class="border-top-0">Size</th>
                                <th scope="col" class="border-top-0">Price</th>
                                <th scope="col" class="border-top-0">Designer</th>
                                {{-- <th scope="col">Description</th> --}}
                                <th scope="col" class="border-top-0">
                                    <a href="{{ route('admin.create') }}" type="button" class="btn btn-sm float-right" style="background-color:#26191B;color:#faf4ff;">Add Collection</a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($gowns as $gown )
                            <tr>
                                {{-- <th scope="row"> <input type="checkbox"></th> --}}
                                <td><img src="{{ URL::asset($gown->image) }}
                                    " alt="" class="img-responsive" style="width:100px;height:110px" title="Add Collection"></td>
                                <td>{{ $gown->name }}</td>
                                <td>{{ $gown->stocks }}</td>
                                <td>{{ $gown->size }}</td>
                                <td>&#8369 {{ number_format($gown->price, 2, '.', ',') }}</td>
                                <td>{{ $gown->designer->designer_name}}</td>
        
                                <td>
                                    <form action="{{route('admin.destroy',['id'=>$gown->id])}}" method="POST" class="float-right">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-white"><img src="{{ URL::to('/images/icons/delete-gray.png') }}" alt="Delete from collection" style="width:20px;" class="" title="Delete Collection"> </button>
                                    </form>

                                    <a class="btn btn-white float-right" href="{{route('admin.edit',['id'=>$gown->id])}}"><img src="{{ URL::to('/images/icons/edit-gray.png') }}" alt="Edit Collection" style="width:20px;" title="Edit Collection"></a>

                                    <a class="btn btn-white float-right" data-toggle="modal" data-target="#view-details{{ $gown->id}}" ><img src="{{ URL::to('/images/icons/view.png') }}" alt="View Collection" style="width:20px;" title="View Collection"></a>

                                    
                                    <!-- Modal -->
                                    
                                    <div class="modal fade" id="view-details{{ $gown->id}}" tabindex="-1" role="dialog" data-id="view-details" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg " role="document">
                                            <div class="modal-content">

                                                
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true"> &times;</span>
                                                    </button> 

                                                    <div class="card mb-3 border-0">
                                                        <div class="row no-gutters">
                                                            <div class="col-md-4">
                                                                <img src="{{ URL::asset($gown->image) }}" alt="" class="img-responsive" style="width:250px;height:350px" title="Add Collection">
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="card-body">
                                                                    <h5 class="modal-title" id="exampleModalLabel">{{ $gown->name }}</h5>
                                                                    <p class="card-text">Product ID # {{$gown->product_code}}</p>
                                                                    <p>Available Size: {{$gown->size}}</p>
                                                                    <p>Available Stocks: {{$gown->stocks}}</p>
                                                                    <p>Description: {{$gown->description}}</p>

                                                                    <footer class="fixed-buttom ">
                                                                        <small class="align-text-bottom text-muted">
                                                                            Tags: <span class="badge badge-dark">{{$gown->category->category_name}}</span> </p>
                                                                        </small>
                                                                    </footer>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- CATEGORIES MANAGEMENT --}}
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="top">
                    <h3 class="mt-5">CATEGORIES <a href="{{ route('admin.create') }}" type="button" class="align-middle"><img src="{{ URL::to('/images/icons/add2.png') }}" alt="" class="img-responsive" style="widht:20px;height:20px" title="Add Category"></a></h3>
              
                   
                  </div>
                  
              
                  <div class="card rounded-0" style="border:2px solid #EF8D8C;">
                      <div class="card-body">
                          <table class="table">
                              <thead>
                                <tr>
                                  <th scope="col" style="border-top:none;"></th>
                                  <th scope="col" style="border-top:none;">Category Name</th>
                                  <th scope="col" class="float-right" style="border-top:none;">Action</th>
                                </tr>
                              </thead>
                              <tbody>
              
                              @foreach ($categories as $category)
                                  
                              
                                <tr>
                                  <th scope="row"><input type="checkbox"></th>
                                  <td>{{ $category->name}}</td>
                                  <td>
                                      <div class="text-right">
                                          <form method="post" action="" class="float-right">
                                            @csrf
                                            @method('DELETE')
                                            <input type='hidden' name='' value=''>
                                            <button type="submit" class="btn btn-white" href=""><img src="{{ URL::to('/images/icons/delete-gray.png') }}" alt="Delete" style="width:20px;" title="Delete Item"> </button>
                                          </form>
                                                          
                                          <form method="post" action="" class="float-right">
                                              @csrf
                                              <input type="hidden" name="gown_id" value=''>
                                              <button type="submit" class="btn btn-white">
                                                  <img src="{{ URL::to('/images/icons/edit-gray.png') }}" alt="Edit Cart" style="width:20px;" class="text-right" title="Edit Cart">
                                              </button>
                                          </form>
                                          
                                                                                
                                             
                                           </div>
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                      </div>
                  </div>
            </div>


        </div>

    </div>



    <script>
        function modal(){
            $()
        }
    </script>
                
                
            
    @endcan

@endsection