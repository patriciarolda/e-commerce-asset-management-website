@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{route('pickup.update',['id'=>$pickups->id])}}"  >    
        @csrf
        @method('PATCH')
        <div class="card h-100">
            <div class="row mt-3">
                <div class="col-md-6">
                    <h5 class="text-center mt-2">Order Details</h5>

                    <div class="form-group mb-3 ">
                        <label for="qty" class="col-md-4 col-form-label text-md-right">Quantity:</label>
                        <input type="number" class="col-md-6 input-underline w-100" min="1" value="{{$pickups->qty}}" name="qty">
                    </div>

                    <div class="form-group mb-3">
                        <label for="event-date"  class="col-md-4 col-form-label text-md-right">Event Date:</label>
                        <input type="date" id="datefield"  class="input-underline w-50" min="" name="event_date" value="{{$pickups->event_date}}">
                    </div>

                    <div class="form-group mb-3 ">
                        <label for="requirements" class="col-md-4 col-form-label text-md-right">Additional Requirements:</label>
                        <textarea name="requirements" rows="5" placeholder="Please write it here." class="w-100 col-md-6">{{$pickups->requirements}}</textarea>   
                    </div>
                </div>

                <div class="col-md-6">
                    <h5 class="text-center mt-2">Pick-up Information</h5>
                    <div class="form-group">
                        <p class="text-danger text-center   "><em><small>Note: We are open from Tues to Sun, 9:00am - 10:00pm</small></em></p>
                        <br>

                        <label for="pick_up_date"  class="col-md-4 col-form-label text-md-right">Pick-Up Date:</label>
                        <input type="date" id="datefield"  class="input-underline w-50" min="" name="pick_up_date" value="{{$pickups->pick_up_date}}">
                    </div>

                    <div class="input-group-sm">
                        <label for="pick-up-time" class="col-md-4 col-form-label text-md-right">Time</label>
                        
                        <select class="custom-select input-underline w-100 col-md-6" id="time" name="pick_up_time">
                            <option name="pick_up_time" value="{{$pickups->pick_up_time}}" {{ $pickups->pick_up_time == $pickups->pick_up_time ? 'selected' : '' }}>{{$pickups->pick_up_time    }}</option>
                            <option value="10:00am">10:00am</option>
                            <option value="11:00am">11:00am</option>
                            <option value="1:00pm">1:00pm</option>
                            <option value="2:00pm">2:00pm</option>
                            <option value="3:00pm">3:00pm</option>
                            <option value="4:00pm">4:00pm</option>
                            <option value="5:00pm">5:00pm</option>
                            <option value="6:00pm">6:00pm</option>
                            <option value="7:00pm">7:00pm</option>
                            <option value="8:00pm">8:00pm</option>
                        </select>
                    </div>

                    <div class="input-group-sm">
                    <label for="pick-up-pax" class="col-md-4 col-form-label text-md-right">Name:</label>
                    
                        <input type="text" class=" col-md-6 custom-select input-underline w-100" min="" name="pick_up_name" value="{{$pickups->pick_up_name}}" ><br>
                    </div>

                    <div class="input-group-sm">
                        <label for="pick-up-number" class="col-md-4 col-form-label text-md-right">Phone number:</label>
                        <input type="number" class="col-md-6 custom-select input-underline w-100 mb-4" name="pick_up_number" value="{{$pickups->pick_up_number}}">
                    </div>
                </div>  
            </div>

            <input type="text"  value="{{$pickups->gown_id}}" name="gown_id" hidden>


            <div class="text-center">
                <button type="submit" class="btn all-button rounded-0 w-50 my-3 ">Update</button> 
            </div>  

        </div>

        
        
    </form>
    
</div>

@endsection