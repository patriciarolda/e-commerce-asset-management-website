@extends('layouts/app')


@section('content')

<style>
    
</style>
<div class="container">
    
    <h1>SHOPPING CART</h1>

    @if (!empty(session('cart')))
        <div class="row">
            <div class="col-md-8">
                <div class="card rounded-0">
                    <div class="card-body">
                        <table class="table table-sm" >
                            <thead >
                                
                                <th scope="col" style="border-top:none;width:25%;" >ITEMS</th>
                                <th scope="col" style="border-top:none;width:40%;">DESCRIPTION</th>
                                <th scope="col" style="border-top:none;width:20%;">PRICE</th>
                                <th scope="col" style="border-top:none;width:20%;">QTY</th>
                                <th scope="col" style="border-top:none;width:20%;">SUBTOTAL</th>
                            
                            </thead>
                            <tbody>
                            
                            @php
                            $total = 0;
                            @endphp


                            @foreach ($gowns as $gown)

                                @php
                                    $subtotal = $gown->price * session('cart.'.$gown->id.'.quantity');
                                    $total += $subtotal;
                                    $delivery = 5000;
                                    $delivery += $total;
                                @endphp
                        
                            
                                    <tr>
                                        <td>
                                            
                                            <img src="{{ URL::asset($gown->image) }}" alt="{{ $gown->name }}" class="img-fluid" title="{{ $gown->name}}" style="width:150px;height:200px;">
                                        </td>
                                        
                                        <td>
                                            <div class=" h-25 d-inline-block"><a href="{{route('user.show',['id'=>$gown->id])}}" class="pink-text text-decoration-none"> <h5 class=" card-title h6">{{ $gown->name }}</a></div>

                                            <div class="h-25"><small><strong>Product Code: </strong>{{ $gown->product_code }}</small></div>

                                            <div class="h-25 font-weight-light"><small><strong>Size:</strong> {{ $gown->size }}</small></div>

                                            <div class="h-25 font-weight-light"><small><strong>Event Date:</strong>  {{ session("cart.".$gown->id.".date") }}
                                            </small></div>
                                            

                                            <div class="h-25 font-weight-light"><small><strong>Additional Requirements: </strong><br>{{ session("cart.".$gown->id.".requirements") }}</small></div>
                                        </td>

                                        <td><small>
                                            &#8369 {{ number_format($gown->price, 2, '.', ',') }}
                                        </small></td>

                                        <td>
                                            <div class="md-form form-sm">
                                            <input type="number" id="inputSMEx" class="form-control form-control-sm" name="qty" value='{{ session("cart.".$gown->id.".quantity") }}'>
                                            </div>                                        
                                        </td>
                                    
                                        <td class='text-right'  ><small>
                                           <p >&#8369 {{ number_format($subtotal, 2, '.', ',') }}</p> 
                                        </small></td>
                                       
                                    </tr>

                                    <tr >
                                        <td colspan="5" style="border-top:none">
                                            <div class="text-right">
                                            
                                            <form method="post" action="{{route('cart.edit')}}" class="float-right">
                                                @csrf
                                                <input type="hidden" name="gown_id" value='{{ $gown->id }}'>
                                                <button type="submit" class="btn btn-white">
                                                    <img src="{{ URL::to('/images/icons/edit-gray.png') }}" alt="Edit Cart" style="width:20px;" class="text-right" title="Edit Cart">
                                                </button>
                                            </form>
                                            
                                                                                  
                                                <form method="post" action="{{ route('cart.remove')}}" class="float-right">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input type='hidden' name='gown_id' value='{{ $gown->id }}'>
                                                    <button type="submit" class="btn btn-white" href=""><img src="{{ URL::to('/images/icons/delete-gray.png') }}" alt="Delete" style="width:20px;" title="Delete Item"> </button>
                                                </form>
                                             </div>
                                        </td>
                                    </tr>   
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card rounded-0 mt-1">
                    <div class="container my-2 mx-2">
                        <p class="h-25 "><small>*A confirmation email will be sent to your email box (the yahoo email address can not receive our email, please leave other email addresses & check your junk mail as well) in 24 work hours after placing an order. Please reply the email as soon as possible since we can’t make the dress without your confirmation.</small></p>

                        <a type="button" href="{{route('user.collection')}}" class="btn all-button rounded-0 float-right ml-2">Continue Shopping</a>

                        <a type="button" href="{{ route('cart.empty')}}" class="btn all-button rounded-0 float-right ml-2" >Clear Shopping Cart</a>
                    </div>
                </div>
            </div>

            {{-- CART SUMMARY   --}}
            <div class="col-md-4">
                <div class="card rounded-0 px-3 py-3">
                    <h4>SUMMARY</h4>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col" style="width:50%;">Subtotal</th>
                            <th scope="col" style="width:50%;">&#8369 {{ number_format($total, 2) }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">
                                Security Fee <br>
                                    <small>(Refundable upon return) <br>
                                </small>
                            </th>
                            <td><small>&#8369 5,000.00 x     </small></td>
                        </tr>

                        <tr>
                            <th>Order Total</th>
                            <th>&#8369 {{ number_format($delivery, 2) }}</th>
                        </tr>

                        <tr>
                            <th colspan="2">
                                <div>
                                    <a data-toggle="collapse" href="#multiCollapseExample1" role="button" class="text-decoration-none pink-text" id="open-close-toggle"><small>Pick-up or Deliver?</small></a>
                                </div>
                                
                              
                                    <input type="radio" id="pick-up" name="pod2" value="pick-up" onclick="showPickUp();">
                                    <label for="pick-up"> I prefer to pick it up.</label>

                                    <br>
                                    <input type="radio" id="deliver" name="pod2" value="deliver" onclick="showDeliver();">
                                    <label for="deliver"> Deliver it to me.</label>
                                </div>                          
                            </th>
                        </tr>
                      
                         {{-- PROCEED TO CHECKOUT --}}
                        <tr  id="del" hidden>
                            

                            {{-- REFUND AND DELIVERY INFORMATION --}}
                            <th  colspan="2" >
                                <h5>DELIVERY INFORMATION</h5>
                                <form action="{{ route('cart.checkout')}}" action="d-inline" method="post">
                                    @csrf
                                    @method('POST')

                                    <label for="address">Please write your complete address:</label>
                                    <div class="input-group-sm">

                                        <textarea name="address" rows="5" placeholder="Complete address" class="w-100" required>{{old('address')}}</textarea>
                                      
                                        
                                    </div>

                                    <div class="text-center">
                                        <input type="hidden" name="total" value="{{ $total }}">
                                        <button class="btn all-button rounded-0 w-100 mt-3">Proceed to Checkout</button>   
                                    </div>                    
                                </form>

                                <p class="h-25"><small>Please read first before you proceed:</small></p>

                                {{-- REFUND TERMS --}}
                                <input type="checkbox" required><a href="" class="pink-text h-25 text-decoration-none" data-toggle="modal" data-target="#refund"><small> Refund Terms and Condition.</small></a> 
                                
                                    <!-- Modal -->
                                    <div class="modal fade" id="refund" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                        <div class="modal-lg modal-dialog modal-dialog-scrollable" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalScrollableTitle">REFUND POLICY</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>To reduce last-minute cancellations and the risk of "chargebacks" from customers, it is always a good idea to have your customers agree to your cancellation and refund policy. This should be attached to the customers' order for future reference. Occasion makes this easy for you and your customers.</p>
                                                
                                                <p> In this article, we will help you define your cancellation and refund policy. Let's start by answering the following questions:</p>
                                            
                                                <ul>
                                                    <li>Do you want to give customers a refund?</li>
                                                    <li>When do they have to inform you by before the actual event date starts to cancel?</li>
                                                    <li>Do you want to keep their payment and give them store credit instead?</li>
                                    
                                                </ul>
                                                
                                                <p>By answering the questions above, you can come up with some very simple and basic policies, like this one: To receive a refund, customers must notify at least 4 days before the start of the event. In all other instances, only a store credit is issued.</p>
                                                
                                                
                                                <p>Below are six great examples of cancellation and refund policies:</p>
                                                
                                                <ol>
                                                    <li>Due to limited seating, we request that you cancel at least 48 hours before a scheduled class. This gives us the opportunity to fill the class. You may cancel by phone or online here. If you have to cancel your class, we offer you a credit to your account if you cancel before the 48 hours, but do not offer refunds. You may use these credits towards any future class. However, if you do not cancel prior to the 48 hours, you will lose the payment for the class. The owner has the only right to be flexible here.</li>
                                                    <li>Cancellations made 7 days or more in advance of the event date, will receive a 100% refund. Cancellations made within 3 - 6 days will incur a 20% fee. Cancellations made within 48 hours to the event will incur a 30% fee.</li>
                                                    <li>I understand that I am holding a spot so reservations for this event are nonrefundable. If I am unable to attend I understand that I can transfer to a friend.</li>
                                                    <li>If your cancellation is at least 24 hours in advance of the class, you will receive a full refund. If your cancellation is less than 24 hours in advance, you will receive a gift certificate to attend a future class. We will do our best to accommodate your needs.</li>
                                                    <li>You may cancel your class up to 24 hours before the class begins and request to receive a full refund. If cancellation is made day of you will receive a credit to reschedule at a later date. Credit must be used within 90 days.</li>
                                                    <li>You may request to cancel your ticket for a full refund, up to 72 hours before the date and time of the event. Cancellations between 25-72 hours before the event may transferred to a different date/time of the same class. Cancellation requests made within 24 hours of the class date/time may not receive a refund nor a transfer. When you register for a class, you agree to these terms.</li>
                                                </ol>
                                                
                                            </div>
                                            <div class="modal-footer">
                                            
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                {{-- END:REFUND TERMS --}}

                                <br>
                                {{-- DELIVERY INSTRUCTION --}}
                                <input type="checkbox" required><a href="" class="pink-text h-25 text-decoration-none" data-toggle="modal" data-target="#deliver-modal"><small> Delivery instruction</small></a> 
                                
                                    <!-- Modal -->
                                    <div class=" modal fade" id="deliver-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                        <div class="modal-lg modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">DELIVERY INTRUCTIONS</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div class="modal-body">
                        
                                                    <h3><i class="fas fa-truck"></i>Carriers</h3>
                                                    <p>We use the following carriers to deliver our orders:</p>
                                                    <ul>
                                                    <li>USPS</li>
                                                    <li>FEDEX</li>
                                                    <li>DHL</li>
                                                    </ul>
                                                
                                                    <h3><i class="fas fa-barcode"></i>Order Tracking</h3>
                                                    <p>If a tracking # is provided by the shipping carrier, we will update your order with the tracking information. Please note that some orders using 1st Class USPS mail will not have tracking numbers.</p>
                                                
                                                    <h3><i class="fas fa-tags"></i>Shipping Rates</h3>
                                                    <p>The rate charged for the shipping of your order is based on the weight of your products, and your location. Before the final checkout page you will be shown what the cost of shipping will be, and you will have a chance to not place your order if you decide not to.</p>
                                                
                                                    <h3><i class="fas fa-store-alt-slash"></i>Back Orders</h3>
                                                    <p>If an item goes on back order we will ship you the part of your order that is in stock. When the item becomes available we will ship you the rest of your order. You will not be charged any additional shipping and handling for the second shipment.</p>
                                                </div>
                                                    
                                                </div>
                                                    <div class="modal-footer">
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                {{-- END:DELIVERY INTRUCTION --}}
                            
                               
                                    
                                 
                            </th>
                        </tr>
                        
                        {{-- REQUEST TO RESERVE --}}
                        <tr id="pick" hidden>   
                            <th colspan="2">                         
                                {{-- DATE OF PICK UP --}}
                                <h5>PICK-UP INFORMATION:</h5>
                                <form action="{{route('cart.pickup')}}" method="POST">
                                @csrf
                                
                                    <div class="form-group mb-3">
                                        <p class="text-danger"><em><small>Note: We are open from Tues to Sun, 9:00am - 10:00pm</small> </em></p>
                                        <label for="pick-up">Set an appointment for gown pick-up:</label><br>

                                        <label for="pick_up_date">Date:</label>
                                        <div class="input-group-sm">
                                            <input type="date" id="datefield"   class="custom-select input-underline w-100" min="" name="pick_up_date" value="{{old('pick_up_date')}}" required>
                                    </div>

                                    <br>
                                            
                                    <label for="pick-up-time">Time</label>
                                    <div class="input-group-sm">
                                        <select class="custom-select input-underline w-100" id="time" name="pick_up_time">
                                        <option alue="{{old('pick_up_time')}}" selected disabled>Choose...</option>
                                        <option value="10:00am">10:00am</option>
                                        <option value="11:00am">11:00am</option>
                                        <option value="1:00pm">1:00pm</option>
                                        <option value="2:00pm">2:00pm</option>
                                        <option value="3:00pm">3:00pm</option>
                                        <option value="4:00pm">4:00pm</option>
                                        <option value="5:00pm">5:00pm</option>
                                        <option value="6:00pm">6:00pm</option>
                                        <option value="7:00pm">7:00pm</option>
                                        <option value="8:00pm">8:00pm</option>
                                        </select>
                                    </div>

                                    <br>

                                    <label for="pick-up-pax">Name of the person to pick-up the gown:</label>
                                    <div class="input-group-sm">
                                        <input type="text" class="custom-select input-underline w-100" min="" name="pick_up_name" value="{{old('pick_up_name')}}" ><br>

                                        <label for="pick-up-number">Phone number:</label>
                                        <input type="number" class="custom-select input-underline w-100 mb-4" name="pick_up_number" value="{{old('pick_up_number')}}" required>
                                    </div>
                                    
                                    <p class="h-25"><small>Please read first before you proceed:</small></p> 

                                    {{-- MODAL: RESERVATION --}}
                                    <input type="checkbox" name="pick_up_checkbox"><a href="" class="pink-text h-25 text-decoration-none" data-toggle="modal" data-target="#pick-up-modal"><small>For Pick-Up Reservation Terms and Condition</small></a>

                                    <div class="modal fade" id="pick-up-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                        <div class=" modal-lg modal-dialog modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">PICK-UP TERMS AND CONDITIONS</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                </div>
                                                    <div class="modal-body">

                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- END MODAL: RESERVATION --}}
                                        <input type="date" name="return_date" hidden>
                                        <div class="text-center">
                                            {{-- <input type="hidden" name="total"> --}}
                                            <button class="btn all-button rounded-0 w-100">Request to Reserve</button> 
                                        </div>    
                                    </div>                  
                                </form>
                                       
                                        
                                    
                    
                              
                                

                                
                            </th>
                        </tr>
                        </tbody>
                    </table>

                    <form action="/cart/checkout/paypal" hidden id="form-checkout-paypal" method="post">
                        @csrf
                    </form>

                    <div id="paypal-button-container" class="mt-2"></div>
                    
                </div>
            </div>
        </div>

        @else
            <div class="container vh-100">
                <p>You have no items in your shopping cart.</p>
                <a class="h6 pink-text" href="{{route('user.collection')}}">Click here to continue shopping.</a>


       
    @endif
</div>

<script>
    function showPickUp(){
        document.getElementById('pick').hidden =false;
        document.getElementById('del').hidden = true;
    }
    function showDeliver(){
        document.getElementById('del').hidden =false;
        document.getElementById('pick').hidden =true;
    }

</script>


<script>

    

    //PAYPAL
    const btnPaypalStyle = {
        height: 38,
        shape: 'rect',
        layout: 'horizontal'
    };
    
    paypal.Buttons({ style:btnPaypalStyle,
        createOrder: function(data, actions) {
            // Sets up transaction details including amount and item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: 
                    }
                }]
            })
        },  
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                // This function shows a transaction success message to your buyer.
                alert('Transaction completed by ' + details.payer.name.given_name);
                document.querySelector('#form-checkout-paypal').submit();
            })
        }
     }).render('#paypal-button-container');


    
    

    

</script>

@endsection
