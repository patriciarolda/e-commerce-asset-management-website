@extends('layouts/collection')

@section('collection')

<style>.hidden{ display: none; }</style>

<div class="container">
    <div class="card" style="border:none">
        <div class="row">

            {{-- COLUMN 1 --}}
            <div class="col-md-6">
                <img src="{{ URL::asset($gown->image) }}" alt="{{ $gown->name }}" class="img-fluid" title="{{ $gown->name}}" style="width:382.50px;height:510px;">
                
            </div>

            {{-- COLUMN 2 --}}
            <div class="col-md-6">
                <small>PID#: {{$gown->product_code}}</small> <small class="font-weight-lighter float-right">Available stock: {{$gown->stocks}}</small>
                <p class="font-weight-bold">{{$gown->name}}</p>
                <p>&#8369 {{ number_format($gown->price, 2, '.', ',') }}</p>
                <p>Available size: {{$gown->size}}</p>


                {{-- ADD TO CART FORM --}}
                <form action="{{route('cart.add',['id'=>$gown->id])}}" method="POST" >
                    @csrf
                    {{-- SIZE --}}
                    <div class="form-group mb-3 ">
                        <label for="qty">How many do you need?</label>
                        <input type="number" class="input-underline w-100" min="1" value="{{ session("cart.".$gown->id.".quantity") }}" name="qty" required>
                    </div>

                    {{-- DATE --}}
                    <div class="form-group mb-3">
                        <label for="event-date">When is the event?</label>
                        <input type="date" id="datefield"  class="input-underline w-50" min="" name="date" value="{{ session("cart.".$gown->id.".date") }}" required>
                    </div>

                    {{-- FAST TRACK OPTION --}}
                    {{-- <div class="form-group mb-3">
                        <label for="event-date">Which do you prefer? (Pick-up or Deliver)</label>
                            <select name="pod"  required {{ (collect(old("pod"))) }}>
                                <option value="Select Option" {{ (collect(old("pod"))) }} disabled selected required>Select Options</option>
                                <option value="Delivery" {{ (collect(old("pod"))) }}required >Delivery</option>
                                <option value="Pick-up"   {{ (collect(old("pod"))) }} required>Pick-up</option>
                            </select>
                    </div> --}}
                
                
                    {{-- ADDITIONAL REQUIREMENTS --}}
                    <div class="form-group mb-3 ">
                        <label for="requirements">Additional Requirements:</label><br>
                        <textarea name="requirements" rows="5" placeholder="Please write it here." class="w-100" required>{{old('requirements')}} {{ session("cart.".$gown->id.".requirements") }}</textarea>
                        
                    </div>

                    {{-- <div class="card my-4" id="pickup" hidden>
                        {{-- DATE OF PICK UP --}}
                        {{-- <form action="" method="">
                            <div class="form-group mb-3 px-3 py-1">
                                <h5>PICK-UP INFORMATION:</h5>
                                <p class="text-danger"><em><small>Note: We are open from Tues to Sun, 9:00am - 10:00pm</small> </em></p>
                                <label for="pick-up">Set an appointment for gown pick-up:</label><br>
                                <label for="Date">Date:</label>
                                <input type="pick-up-date" id="datefield"  class="input-underline w-50" min="" name="pick-up-date" value="{{old('date')}}" required><br>

                                <label for="pick-up-time">Time</label>
                                <input type="pick-up-time" class="input-underline w-50" min="" name="pick-up-time" value="{{old('pick-up-time')}}" required>

                                <label for="pick-up-pax">Name of the person to pick-up the gown:</label>
                                <input type="text" class="input-underline w-50" min="" name="date" value="{{old('pick-up-pax')}}" required><br>

                                <label for="pick-up-number">Phone number:</label>
                                <input type="pick-up-number" class="input-underline w-50 mb-4" max="11" name="pick-up-number" value="{{old('pick-up-pax')}}" required>
                                
                                <div class="text-center">
                                    <button class="btn all-button btn-sm rounded-0 " type="submit">Request for Reservation</button>
                                </div>
                                
                            </div>
                        </form>
                    </div> --}}

                    

                    <div class="row text-center">
                        <div class="col-md-6">
                            <button class="btn all-button btn-sm rounded-0" type="submit">ADD TO CART</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn all-button btn-sm rounded-0" type="submit">ADD TO WISHLIST</button>
                        </div>
                    </div>                   
                </form>
                {{-- END OF ADD TO CART --}}
            </div>
        </div>
    </div>

    <div class="card mt-5" style="border:none">
        <div>
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link " type="button" data-toggle="collapse" data-target="#collapseOne" style="color:#ea728c">
                        MUST-READ TIPS
                        </button>
                    </h2>
                </div>
            
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p style="color:#cc0e74"> 1. How long does it take to receive a dress after ordering?</p>
                
                        <p>All our dresses are made-to-order. Usually orders take 4-5 weeks to process before they ship out. And it takes 3-5 working days for delivery.</p>   
    
                        <p style="color:#cc0e74">2. Can I check the color and fabric in person?</p>
                        <p>Yes! We provide color swatch service to those customers who feel uncertain about the colors. Simply click Swatches after the displayed colors, you will be directed to the color sample page.</p>
                        <p> Please note that colors may vary due to different batches of fabric, so we highly recommend ordering dresses sooner after you settle on the color.</p>
                        <p>And please keep in mind that even the same color may vary in different types of fabrics. Please make sure you are OK with the color shading if you want mix-and-match bridesmaid dresses in same color.</p>
    
                        <p style="color:#cc0e74">3. How can I avoid color variations?</p>
                        <p>Usually we suggest putting all dresses together on one order to avoid dye lot variations by using the same batch of fabric. However, it’s OK to order bridesmaid dresses separately if all orders can be placed in 2 weeks. We will collect all orders together and make sure them from same dye lot.</p>
    
                        <p style="color:#cc0e74">4. How do I know which size is right for me?</p>
                        <p>We understand the importance of sizing for such an event! So we provide both standard sizing and custom sizing service. Please follow our step by step measurement guide and compare your body measurements to the size chart and select the perfect size.</p>
                        <p>If you are in between sizes, you can just choose Custom Size (no extra cost). We will leave additional fabric in seams part in case there is any alteration.</p>
    
                    </div>
                    
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color:#ea728c">
                            DETAILS
                            </button>
                        </h2>
                    </div>

                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>{{$gown->description}}</p>
                            
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="color:#ea728c">
                            REVIEWS
                            </button>
                        </h2>
                    </div>

                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            **CURRENTLY IN PROGRESS**
                        </div>
                    </div>
                </div>

            </div>
        </div>  
    </div>
</div>
                 

<script>
    // TO PROVIDE RESTRICTIONS WITH DATE INPUT
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();
    if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    today = yyyy+'-'+mm+'-'+dd;
    document.getElementById("datefield").setAttribute("min", today);

    
</script>



@endsection