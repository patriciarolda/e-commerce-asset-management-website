<?php

use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actions')->insert(
            [
                [
                    'status'=>'On-request',
                    'created_at' => now()
                ],
                [
                    'status'=>'Ready for Pick-up',
                    'created_at' => now()
                ],

                [
                    'status'=>'Received',
                    'created_at' => now()
                ],

                [
                    'status'=>'Returned',
                    'created_at' => now()
                ] ,
                [
                    'status'=>'Ready for Delivery',
                    'created_at' => now()
                ] ,
                [
                    'status'=>'Cancelled',
                    'created_at' => now()
                ],
                [
                    'status'=>'Denied',
                    'created_at' => now()
                ] 
            ]
        );
    }
}
