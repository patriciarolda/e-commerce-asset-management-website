<?php

use Illuminate\Database\Seeder;

class DesignersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designers')->insert(
            [
                [
                    'designer_name'=>'Bianca Aromin',
                    'created_at' => now()
                ],

                [
                    'designer_name'=>'Yashin Lapay',
                    'created_at' => now()
                ],

                [
                    'designer_name'=>'Ali King',
                    'created_at' => now()
                ], 
            ]
        );
    }
}
