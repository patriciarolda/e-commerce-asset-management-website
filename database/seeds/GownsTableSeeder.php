<?php

use Illuminate\Database\Seeder;

class GownsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gowns')->insert(
            [

                // WEDDING
                [
                    'name'              => 'Elegant White Wedding Dress',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '10000.00',
                    'description'       => '
                                            Model: Mai Suria
                                            Hmua: Joreen Jadia
                                            Photo: Faith Lidres
                                            
                                            SN 6740 - ballgown
                                            SN 7299 - butterfly sleeves
                                            Size: Large
                                            Color: black',
                    'image'             => '/images/gowns/wedding/wedding3.jpg',
                    'category_id'       => '1',
                    'sub_category_id'   => '1',
                    'gown_type_id'      => '1',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'White Tulle Wedding Gown',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '10000.00',
                    'description'       => '
                                            Model: Mai Suria
                                            Hmua: Joreen Jadia
                                            Photo: Faith Lidres
                                            
                                            SN: Ballgown
                                            SN 7265 - butterfly sleeves
                                            Size: Large
                                            Color: white',
                    'image'             => '/images/gowns/wedding/wedding1.jpg',
                    'category_id'       => '1',
                    'sub_category_id'   => '1',
                    'gown_type_id'      => '1',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'White Tulle Wedding Gown',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '10000.00',
                    'description'       => '
                    Notice:
                    1. Estimated production time 4-5 weeks.
                    2. Some of the dress colors above are photoshoped images.
                    
                    Fabric: Tulle
                    Photographed In: Dutsy Rose
                    Silhouette: A-line
                    Length: Full Length
                    Back: Zipper
                    Train: No
                    * If you need more than one dress, please put all the dresses in one order to avoid dye lot variations.',
                    'image'             => '/images/gowns/wedding/wedding2.jpg',
                    'category_id'       => '1',
                    'sub_category_id'   => '1',
                    'gown_type_id'      => '1',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Whimsical White Wedding Dress',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '12000.00',
                    'description'       => '
                    Notice:
                    1. Estimated production time 4-5 weeks.
                    2. Some of the dress colors above are photoshoped images.
                    
                    Fabric: Tulle
                    Photographed In: Dutsy Rose
                    Silhouette: A-line
                    Length: Full Length
                    Back: Zipper
                    Train: No
                    * If you need more than one dress, please put all the dresses in one order to avoid dye lot variations.',
                    'image'             => '/images/gowns/wedding/wedding4.jpg',
                    'category_id'       => '1',
                    'sub_category_id'   => '1',
                    'gown_type_id'      => '1',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Classic White Wedding Dress',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '13000.00',
                    'description'       => '
                    Notice:
                    1. Estimated production time 4-5 weeks.
                    2. Some of the dress colors above are photoshoped images.
                    
                    Fabric: Tulle
                    Photographed In: Dutsy Rose
                    Silhouette: A-line
                    Length: Full Length
                    Back: Zipper
                    Train: No
                    * If you need more than one dress, please put all the dresses in one order to avoid dye lot variations.',
                    'image'             => '/images/gowns/wedding/wedding5.jpg',
                    'category_id'       => '1',
                    'sub_category_id'   => '1',
                    'gown_type_id'      => '1',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Elegant Pale Wedding Dress',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '13000.00',
                    'description'       => '
                    Notice:
                    1. Estimated production time 4-5 weeks.
                    2. Some of the dress colors above are photoshoped images.
                    
                    Fabric: Tulle
                    Photographed In: Dutsy Rose
                    Silhouette: A-line
                    Length: Full Length
                    Back: Zipper
                    Train: No
                    * If you need more than one dress, please put all the dresses in one order to avoid dye lot variations.',
                    'image'             => '/images/gowns/wedding/wedding6.jpg',
                    'category_id'       => '1',
                    'sub_category_id'   => '1',
                    'gown_type_id'      => '1',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Triangle Whitish Wedding Dress',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '13000.00',
                    'description'       => '
                    Notice:
                    1. Estimated production time 4-5 weeks.
                    2. Some of the dress colors above are photoshoped images.
                    
                    Fabric: Tulle
                    Photographed In: Dutsy Rose
                    Silhouette: A-line
                    Length: Full Length
                    Back: Zipper
                    Train: No
                    * If you need more than one dress, please put all the dresses in one order to avoid dye lot variations.',
                    'image'             => '/images/gowns/wedding/wedding5.jpg',
                    'category_id'       => '1',
                    'sub_category_id'   => '1',
                    'gown_type_id'      => '1',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Filipiniana White Butterfly',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '10000.00',
                    'description'       => '
                                            SN 6204 - long gown
                                            SN 7467 - Butterfly sleeves
                                            Size: Small
                                            Color: white
                                            Materials: Tulle
                                            
                                            Model: Tin Tin Shin
                                            Hmua: Joreen Jadia
                                            Photo: Faith Lidres',
                    'image'             => '/images/gowns/filipiniana/1.jpg',
                    'category_id'       => '5',
                    'sub_category_id'   => null,
                    'gown_type_id'      => '3',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Filipiniana Black Butterfly',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '10000.00',
                    'description'       => '
                                            Model: Mai Suria
                                            Hmua: Joreen Jadia
                                            Photo: Faith Lidres
                                            
                                            SN 6740 - ballgown
                                            SN 7299 - butterfly sleeves
                                            Size: Large
                                            Color: black',
                    'image'             => '/images/gowns/filipiniana/2black-butterfly-filipiniana-ballgown.jpg',
                    'category_id'       => '5',
                    'sub_category_id'   => null,
                    'gown_type_id'      => '3',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Filipiniana White Butterfly',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '10000.00',
                    'description'       => '
                                            Model: Mai Suria
                                            Hmua: Joreen Jadia
                                            Photo: Faith Lidres
                                            
                                            SN: Ballgown
                                            SN 7265 - butterfly sleeves
                                            Size: Large
                                            Color: white',
                    'image'             => '/images/gowns/filipiniana/3gold-filipiniana-ballgown.jpg',
                    'category_id'       => '5',
                    'sub_category_id'   => null,
                    'gown_type_id'      => '3',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],

                [
                    'name'              => 'Filipiniana Black Butterfly',
                    'product_code'      => 'VX12345',
                    'stocks'            => '1',
                    'size'              => '4',
                    'price'             => '10000.00',
                    'description'       => '
                                            Model: Mai Suria
                                            Hmua: Joreen Jadia
                                            Photo: Faith Lidres
                                            
                                            SN 6740 - ballgown
                                            SN 7299 - butterfly sleeves
                                            Size: Large
                                            Color: black',
                    'image'             => '/images/gowns/filipiniana/2black-butterfly-filipiniana-ballgown.jpg',
                    'category_id'       => '5',
                    'sub_category_id'   => null,
                    'gown_type_id'      => '3',
                    'designer_id'       => '1',
                    'created_at' => now()
                ],


               
            ]
        );
    }

}
