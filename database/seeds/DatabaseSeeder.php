<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(
            [
                UsersTableSeeder::class
            ]
        );


        $this->call(
            [
                CategoriesTableSeeder::class
            ]
        );

        $this->call(
            [
                GownTypesSeeder::class
            ]
        );

        $this->call(
            [
                SubCategoriesTableSeeder::class
            ]
        );

        $this->call(
            [
                DesignersTableSeeder::class
            ]
        );

        $this->call(
            [
                GownsTableSeeder::class
            ]
        );

        $this->call(
            [
                ActionsTableSeeder::class
            ]
        );
    }
}
