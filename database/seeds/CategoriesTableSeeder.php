<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [
                    'category_name'=>'Wedding',
                    'created_at' => now()
                ],

                [
                    'category_name'=>'Debut',
                    'created_at' => now()
                ],

                [
                    'category_name'=>'Prom',
                    'created_at' => now()
                ],

                [
                    'category_name'=>'Pageant',
                    'created_at' => now()
                ],

                [
                    'category_name'=>'Filipiniana',
                    'created_at' => now()
                ]    
            ]
        );
    }
}
