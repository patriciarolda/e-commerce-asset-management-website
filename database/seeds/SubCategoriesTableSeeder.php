<?php

use Illuminate\Database\Seeder;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_categories')->insert(
            [
                [
                    'sub_category_name'=>'Brides',
                    'category_id' => '1',
                    'created_at' => now()
                ],

                [
                    'sub_category_name'=>'Groom',
                    'category_id' => '1',
                    'created_at' => now()
                ],

                [
                    'sub_category_name'=>'Groomsmen, Bridesmaid & Maid of Honor',
                    'category_id' => '1',
                    'created_at' => now()
                ],

                [
                    'sub_category_name'=>'Parents',
                    'category_id' => '1',
                    'created_at' => now()
                ],

                [
                    'sub_category_name'=>'Flower girl & ring bearer',
                    'category_id' => '1',
                    'created_at' => now()
                ],

                [
                    'sub_category_name'=>'Parents',
                    'category_id' => '1',
                    'created_at' => now()
                ],

            ]
        );
    }
}
