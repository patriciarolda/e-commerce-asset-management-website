<?php

use Illuminate\Database\Seeder;

class GownTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('gown_types')->insert(
            [
                [
                    'gown_type_name'=>'Long Gown',
                    'created_at' => now()
                ],

                [
                    'gown_type_name'=>'Cocktail',
                    'created_at' => now()
                ],

                [
                    'gown_type_name'=>'Ballgown',
                    'created_at' => now()
                ],

                [
                    'gown_type_name'=>'Others',
                    'created_at' => now()
                ] 
            ]
        );
    }
}
