<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'fname' => 'Patricia',
                    'lname' => 'Rolda',
                    'number'=> '09152867142',
                    'email' => 'roldapatricia@gmail.com',
                    'password' => bcrypt('demo1234'),
                    'role' => 'admin'
                ],

                [
                    'fname' => 'Quarter',
                    'lname' => 'Roque',
                    'number'=> '0912345678',
                    'email' => 'user@mail.com',
                    'password' => bcrypt('demo1234'),
                    'role' => 'user'
                ]
            ]

        );
    }
}
