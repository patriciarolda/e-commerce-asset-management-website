<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pickups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('gown_id');

            // SESSION DATA
                $table->integer('qty');
                $table->date('event_date');
                $table->longText('requirements');
                $table->double('total',10,2);
            // CART PICK-UP RESERVATION DATA
            $table->date('pick_up_date');
            $table->string('pick_up_time');
            $table->string('pick_up_name');
            $table->string('pick_up_number');
            $table->unsignedBigInteger('action_id');
            $table->date('return_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('gown_id')->references('id')->on('gowns');
            $table->foreign('action_id')->references('id')->on('actions');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pickups');
    }
}
